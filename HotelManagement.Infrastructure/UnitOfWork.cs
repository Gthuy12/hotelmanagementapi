﻿using HotelManagement.Domain.Interfaces;

namespace HotelManagement.Infrastructure
{
    public class UnitOfWork: IUnitOfWork
    {
        private DbFactory _dbFactory;
        public UnitOfWork(DbFactory dbFactory)
        {
            _dbFactory = dbFactory;
        }
        public void SaveChanges()
        {
            _dbFactory.DbContext.SaveChanges();
        }
    }
}
