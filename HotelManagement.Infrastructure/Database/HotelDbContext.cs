﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;
using HotelManagement.Domain.Entities;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;

namespace HotelManagement.Infrastructure
{
    public class HotelDbContext : IdentityDbContext<User>
    {
        public DbSet<Booking> Booking { get; set; }
        public DbSet<BookingRoom> BookingRoom { get; set; }
        public DbSet<Client> Client { get; set; }
        public DbSet<Room> Room { get; set; }
        public DbSet<RoomPlan> RoomPlan { get; set; }
        public DbSet<RoomService> RoomService { get; set; }
        public DbSet<Service> Service { get; set; } 
        public DbSet<Checkin> Checkin { get; set; }
        public DbSet<DailyAudit> DailyAudit { get; set; }
        public DbSet<CheckinService> CheckinService { get; set; }
        public DbSet<Checkout> Checkout { get; set; }
        public DbSet<Invoice> Invoice { get; set; }
        public DbSet<InvoiceService> InvoiceService { get; set; }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {

        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
        }
        public HotelDbContext(DbContextOptions<HotelDbContext> options) : base(options)
        {
        }
    }
}
