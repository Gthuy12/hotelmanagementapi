﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using HotelManagement.Domain.Entities;
using Microsoft.AspNetCore.Cryptography.KeyDerivation;

namespace HotelManagement.Infrastructure.Database
{
    public class DbInitializer
    {
        public static void Initialize(HotelDbContext context)
        {
            context.Database.EnsureCreated();
            // 1. Check if database have been seeded already
            if (context.Service.Any())
            {     
                return;
            } 
            
            // 2. Seed data here, check git to get code seed data
        } 
    }
}
