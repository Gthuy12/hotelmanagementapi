﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace HotelManagement.Infrastructure.Migrations
{
    public partial class UpdateCheckoutV2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ServicePrice",
                table: "CheckoutService");

            migrationBuilder.AddColumn<int>(
                name: "InvoiceId",
                table: "Invoice",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Invoice_InvoiceId",
                table: "Invoice",
                column: "InvoiceId");

            migrationBuilder.AddForeignKey(
                name: "FK_Invoice_Invoice_InvoiceId",
                table: "Invoice",
                column: "InvoiceId",
                principalTable: "Invoice",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Invoice_Invoice_InvoiceId",
                table: "Invoice");

            migrationBuilder.DropIndex(
                name: "IX_Invoice_InvoiceId",
                table: "Invoice");

            migrationBuilder.DropColumn(
                name: "InvoiceId",
                table: "Invoice");

            migrationBuilder.AddColumn<string>(
                name: "ServicePrice",
                table: "CheckoutService",
                type: "nvarchar(max)",
                nullable: true);
        }
    }
}
