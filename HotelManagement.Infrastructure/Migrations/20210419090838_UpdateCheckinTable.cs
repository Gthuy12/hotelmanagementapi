﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace HotelManagement.Infrastructure.Migrations
{
    public partial class UpdateCheckinTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Client_Checkin_CheckinId",
                table: "Client");

            migrationBuilder.DropIndex(
                name: "IX_Client_CheckinId",
                table: "Client");

            migrationBuilder.DropColumn(
                name: "CheckinId",
                table: "Client");

            migrationBuilder.CreateTable(
                name: "CheckinClient",
                columns: table => new
                {
                    CheckinsId = table.Column<int>(type: "int", nullable: false),
                    ClientsId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CheckinClient", x => new { x.CheckinsId, x.ClientsId });
                    table.ForeignKey(
                        name: "FK_CheckinClient_Checkin_CheckinsId",
                        column: x => x.CheckinsId,
                        principalTable: "Checkin",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_CheckinClient_Client_ClientsId",
                        column: x => x.ClientsId,
                        principalTable: "Client",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_CheckinClient_ClientsId",
                table: "CheckinClient",
                column: "ClientsId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "CheckinClient");

            migrationBuilder.AddColumn<int>(
                name: "CheckinId",
                table: "Client",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Client_CheckinId",
                table: "Client",
                column: "CheckinId");

            migrationBuilder.AddForeignKey(
                name: "FK_Client_Checkin_CheckinId",
                table: "Client",
                column: "CheckinId",
                principalTable: "Checkin",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
