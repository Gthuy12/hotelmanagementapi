﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace HotelManagement.Infrastructure.Migrations
{
    public partial class UpdateCheckoutV4 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_CheckoutService_Invoice_InvoiceId",
                table: "CheckoutService");

            migrationBuilder.DropForeignKey(
                name: "FK_CheckoutService_Service_ServiceId",
                table: "CheckoutService");

            migrationBuilder.DropPrimaryKey(
                name: "PK_CheckoutService",
                table: "CheckoutService");

            migrationBuilder.RenameTable(
                name: "CheckoutService",
                newName: "InvoiceService");

            migrationBuilder.RenameIndex(
                name: "IX_CheckoutService_ServiceId",
                table: "InvoiceService",
                newName: "IX_InvoiceService_ServiceId");

            migrationBuilder.RenameIndex(
                name: "IX_CheckoutService_InvoiceId",
                table: "InvoiceService",
                newName: "IX_InvoiceService_InvoiceId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_InvoiceService",
                table: "InvoiceService",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_InvoiceService_Invoice_InvoiceId",
                table: "InvoiceService",
                column: "InvoiceId",
                principalTable: "Invoice",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_InvoiceService_Service_ServiceId",
                table: "InvoiceService",
                column: "ServiceId",
                principalTable: "Service",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_InvoiceService_Invoice_InvoiceId",
                table: "InvoiceService");

            migrationBuilder.DropForeignKey(
                name: "FK_InvoiceService_Service_ServiceId",
                table: "InvoiceService");

            migrationBuilder.DropPrimaryKey(
                name: "PK_InvoiceService",
                table: "InvoiceService");

            migrationBuilder.RenameTable(
                name: "InvoiceService",
                newName: "CheckoutService");

            migrationBuilder.RenameIndex(
                name: "IX_InvoiceService_ServiceId",
                table: "CheckoutService",
                newName: "IX_CheckoutService_ServiceId");

            migrationBuilder.RenameIndex(
                name: "IX_InvoiceService_InvoiceId",
                table: "CheckoutService",
                newName: "IX_CheckoutService_InvoiceId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_CheckoutService",
                table: "CheckoutService",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_CheckoutService_Invoice_InvoiceId",
                table: "CheckoutService",
                column: "InvoiceId",
                principalTable: "Invoice",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_CheckoutService_Service_ServiceId",
                table: "CheckoutService",
                column: "ServiceId",
                principalTable: "Service",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
