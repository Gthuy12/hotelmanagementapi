﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace HotelManagement.Infrastructure.Migrations
{
    public partial class InitialCheckinPhaseV1 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "CheckinId",
                table: "Client",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "CheckinId",
                table: "BookingService",
                type: "int",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "Checkin",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CheckinTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    CheckoutTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    ContactName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ContactPhone = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Notes = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    RoomId = table.Column<int>(type: "int", nullable: false),
                    BookingId = table.Column<int>(type: "int", nullable: false),
                    AuthorId = table.Column<string>(type: "nvarchar(450)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Checkin", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Checkin_AspNetUsers_AuthorId",
                        column: x => x.AuthorId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Checkin_Booking_BookingId",
                        column: x => x.BookingId,
                        principalTable: "Booking",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Checkin_Room_RoomId",
                        column: x => x.RoomId,
                        principalTable: "Room",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Client_CheckinId",
                table: "Client",
                column: "CheckinId");

            migrationBuilder.CreateIndex(
                name: "IX_BookingService_CheckinId",
                table: "BookingService",
                column: "CheckinId");

            migrationBuilder.CreateIndex(
                name: "IX_Checkin_AuthorId",
                table: "Checkin",
                column: "AuthorId");

            migrationBuilder.CreateIndex(
                name: "IX_Checkin_BookingId",
                table: "Checkin",
                column: "BookingId");

            migrationBuilder.CreateIndex(
                name: "IX_Checkin_RoomId",
                table: "Checkin",
                column: "RoomId");

            migrationBuilder.AddForeignKey(
                name: "FK_BookingService_Checkin_CheckinId",
                table: "BookingService",
                column: "CheckinId",
                principalTable: "Checkin",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Client_Checkin_CheckinId",
                table: "Client",
                column: "CheckinId",
                principalTable: "Checkin",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_BookingService_Checkin_CheckinId",
                table: "BookingService");

            migrationBuilder.DropForeignKey(
                name: "FK_Client_Checkin_CheckinId",
                table: "Client");

            migrationBuilder.DropTable(
                name: "Checkin");

            migrationBuilder.DropIndex(
                name: "IX_Client_CheckinId",
                table: "Client");

            migrationBuilder.DropIndex(
                name: "IX_BookingService_CheckinId",
                table: "BookingService");

            migrationBuilder.DropColumn(
                name: "CheckinId",
                table: "Client");

            migrationBuilder.DropColumn(
                name: "CheckinId",
                table: "BookingService");
        }
    }
}
