﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace HotelManagement.Infrastructure.Migrations
{
    public partial class RefactorCheckin : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Booking_AspNetUsers_AuthorId",
                table: "Booking");

            migrationBuilder.DropForeignKey(
                name: "FK_Checkin_AspNetUsers_AuthorId",
                table: "Checkin");

            migrationBuilder.RenameColumn(
                name: "AuthorId",
                table: "Checkin",
                newName: "CreatedByAuthor");

            migrationBuilder.RenameIndex(
                name: "IX_Checkin_AuthorId",
                table: "Checkin",
                newName: "IX_Checkin_CreatedByAuthor");

            migrationBuilder.RenameColumn(
                name: "AuthorId",
                table: "Booking",
                newName: "CreatedByAuthor");

            migrationBuilder.RenameIndex(
                name: "IX_Booking_AuthorId",
                table: "Booking",
                newName: "IX_Booking_CreatedByAuthor");

            migrationBuilder.AddForeignKey(
                name: "FK_Booking_AspNetUsers_CreatedByAuthor",
                table: "Booking",
                column: "CreatedByAuthor",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Checkin_AspNetUsers_CreatedByAuthor",
                table: "Checkin",
                column: "CreatedByAuthor",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Booking_AspNetUsers_CreatedByAuthor",
                table: "Booking");

            migrationBuilder.DropForeignKey(
                name: "FK_Checkin_AspNetUsers_CreatedByAuthor",
                table: "Checkin");

            migrationBuilder.RenameColumn(
                name: "CreatedByAuthor",
                table: "Checkin",
                newName: "AuthorId");

            migrationBuilder.RenameIndex(
                name: "IX_Checkin_CreatedByAuthor",
                table: "Checkin",
                newName: "IX_Checkin_AuthorId");

            migrationBuilder.RenameColumn(
                name: "CreatedByAuthor",
                table: "Booking",
                newName: "AuthorId");

            migrationBuilder.RenameIndex(
                name: "IX_Booking_CreatedByAuthor",
                table: "Booking",
                newName: "IX_Booking_AuthorId");

            migrationBuilder.AddForeignKey(
                name: "FK_Booking_AspNetUsers_AuthorId",
                table: "Booking",
                column: "AuthorId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Checkin_AspNetUsers_AuthorId",
                table: "Checkin",
                column: "AuthorId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
