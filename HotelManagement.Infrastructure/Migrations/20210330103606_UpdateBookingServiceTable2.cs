﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace HotelManagement.Infrastructure.Migrations
{
    public partial class UpdateBookingServiceTable2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_BookingService_Service_BookingId",
                table: "BookingService");

            migrationBuilder.CreateIndex(
                name: "IX_BookingService_ServiceId",
                table: "BookingService",
                column: "ServiceId");

            migrationBuilder.AddForeignKey(
                name: "FK_BookingService_Service_ServiceId",
                table: "BookingService",
                column: "ServiceId",
                principalTable: "Service",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_BookingService_Service_ServiceId",
                table: "BookingService");

            migrationBuilder.DropIndex(
                name: "IX_BookingService_ServiceId",
                table: "BookingService");

            migrationBuilder.AddForeignKey(
                name: "FK_BookingService_Service_BookingId",
                table: "BookingService",
                column: "BookingId",
                principalTable: "Service",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
