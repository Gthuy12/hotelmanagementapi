﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace HotelManagement.Infrastructure.Migrations
{
    public partial class UpdateDbCheckin : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_BookingService_Checkin_CheckinId",
                table: "BookingService");

            migrationBuilder.DropIndex(
                name: "IX_BookingService_CheckinId",
                table: "BookingService");

            migrationBuilder.DropColumn(
                name: "CheckinId",
                table: "BookingService");

            migrationBuilder.CreateTable(
                name: "CheckinService",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CheckinId = table.Column<int>(type: "int", nullable: false),
                    ServiceId = table.Column<int>(type: "int", nullable: false),
                    Quantity = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CheckinService", x => x.Id);
                    table.ForeignKey(
                        name: "FK_CheckinService_Checkin_CheckinId",
                        column: x => x.CheckinId,
                        principalTable: "Checkin",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_CheckinService_Service_ServiceId",
                        column: x => x.ServiceId,
                        principalTable: "Service",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_CheckinService_CheckinId",
                table: "CheckinService",
                column: "CheckinId");

            migrationBuilder.CreateIndex(
                name: "IX_CheckinService_ServiceId",
                table: "CheckinService",
                column: "ServiceId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "CheckinService");

            migrationBuilder.AddColumn<int>(
                name: "CheckinId",
                table: "BookingService",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_BookingService_CheckinId",
                table: "BookingService",
                column: "CheckinId");

            migrationBuilder.AddForeignKey(
                name: "FK_BookingService_Checkin_CheckinId",
                table: "BookingService",
                column: "CheckinId",
                principalTable: "Checkin",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
