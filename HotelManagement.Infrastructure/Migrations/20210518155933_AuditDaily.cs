﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace HotelManagement.Infrastructure.Migrations
{
    public partial class AuditDaily : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "DailyAuditId",
                table: "InvoiceDetail",
                type: "int",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "DailyAudit",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    DateOcuppied = table.Column<DateTime>(type: "datetime2", nullable: false),
                    RoomPlanId = table.Column<int>(type: "int", nullable: true),
                    ServicePrice = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    RoomPrice = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    TotalPrice = table.Column<decimal>(type: "decimal(18,2)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DailyAudit", x => x.Id);
                    table.ForeignKey(
                        name: "FK_DailyAudit_RoomPlan_RoomPlanId",
                        column: x => x.RoomPlanId,
                        principalTable: "RoomPlan",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_InvoiceDetail_DailyAuditId",
                table: "InvoiceDetail",
                column: "DailyAuditId");

            migrationBuilder.CreateIndex(
                name: "IX_DailyAudit_RoomPlanId",
                table: "DailyAudit",
                column: "RoomPlanId");

            migrationBuilder.AddForeignKey(
                name: "FK_InvoiceDetail_DailyAudit_DailyAuditId",
                table: "InvoiceDetail",
                column: "DailyAuditId",
                principalTable: "DailyAudit",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_InvoiceDetail_DailyAudit_DailyAuditId",
                table: "InvoiceDetail");

            migrationBuilder.DropTable(
                name: "DailyAudit");

            migrationBuilder.DropIndex(
                name: "IX_InvoiceDetail_DailyAuditId",
                table: "InvoiceDetail");

            migrationBuilder.DropColumn(
                name: "DailyAuditId",
                table: "InvoiceDetail");
        }
    }
}
