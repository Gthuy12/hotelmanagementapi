﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace HotelManagement.Infrastructure.Migrations
{
    public partial class UpdateBookingServiceTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_BookingService_Booking_BookingId",
                table: "BookingService");

            migrationBuilder.DropForeignKey(
                name: "FK_BookingService_Service_ServiceId",
                table: "BookingService");

            migrationBuilder.DropIndex(
                name: "IX_BookingService_ServiceId",
                table: "BookingService");

            migrationBuilder.AlterColumn<int>(
                name: "ServiceId",
                table: "BookingService",
                type: "int",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "BookingId",
                table: "BookingService",
                type: "int",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_BookingService_Booking_BookingId",
                table: "BookingService",
                column: "BookingId",
                principalTable: "Booking",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_BookingService_Service_BookingId",
                table: "BookingService",
                column: "BookingId",
                principalTable: "Service",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_BookingService_Booking_BookingId",
                table: "BookingService");

            migrationBuilder.DropForeignKey(
                name: "FK_BookingService_Service_BookingId",
                table: "BookingService");

            migrationBuilder.AlterColumn<int>(
                name: "ServiceId",
                table: "BookingService",
                type: "int",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AlterColumn<int>(
                name: "BookingId",
                table: "BookingService",
                type: "int",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.CreateIndex(
                name: "IX_BookingService_ServiceId",
                table: "BookingService",
                column: "ServiceId");

            migrationBuilder.AddForeignKey(
                name: "FK_BookingService_Booking_BookingId",
                table: "BookingService",
                column: "BookingId",
                principalTable: "Booking",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_BookingService_Service_ServiceId",
                table: "BookingService",
                column: "ServiceId",
                principalTable: "Service",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
