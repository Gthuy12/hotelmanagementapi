﻿using HotelManagement.Domain.Entities;
using HotelManagement.Domain.RepositoryInterfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace HotelManagement.Infrastructure.Repositories
{
    public class CheckoutRepository : Repository<Checkout>, ICheckoutRepository
    {
        public CheckoutRepository(DbFactory dbFactory) : base(dbFactory)
        {

        }
    }
}
