﻿using HotelManagement.Domain.Entities;
using HotelManagement.Domain.RepositoryInterfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HotelManagement.Infrastructure.Repositories
{
    public class InvoiceServiceRepository : Repository<InvoiceService>, IInvoiceServiceRepository
    {
        public InvoiceServiceRepository(DbFactory dbFactory) : base(dbFactory)
        {

        }
        public void DeleteInvoiceServices(int id)
        {
            DbSet.RemoveRange(DbSet.Where(t => t.InvoiceId == id));
        }
    }
}
