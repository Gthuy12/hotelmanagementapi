﻿using HotelManagement.Domain.Entities;
using HotelManagement.Domain.RepositoryInterfaces;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;

namespace HotelManagement.Infrastructure.Repositories
{
    public class CheckinServiceRepository : Repository<CheckinService>, ICheckinServiceRepository
    {
        public CheckinServiceRepository(DbFactory dbFactory) : base(dbFactory)
        {

        }

        public Task<List<CheckinService>> GetCheckinServices(int id)
        {
            return DbSet.Where(t => t.CheckinId == id).ToListAsync();
        }

        public void DeleteCheckinServices(int id)
        {
            DbSet.RemoveRange(DbSet.Where(t => t.CheckinId == id));
        }
    }
}
