﻿using HotelManagement.Domain.Entities;
using HotelManagement.Domain.RepositoryInterfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HotelManagement.Infrastructure.Repositories
{
    public class RoomPlanRepository : Repository<RoomPlan>, IRoomPlanRepository
    {
        public RoomPlanRepository(DbFactory dbFactory) : base(dbFactory)
        {

        }

        public IQueryable<RoomPlan> GetRoomPlans()
        {
            return DbSet.Include(_ => _.Services);
        }

        public IQueryable<RoomPlan> GetRooms(List<Room> rooms, List<DateTime> dateTimes, RoomPlanType planType)
        {
            return DbSet.Where(_ => rooms.Contains(_.Room) 
                            && dateTimes.Contains(_.DateOcuppied)
                            && _.RoomPlanType == planType);
        }

        public IQueryable<RoomPlan> GetRoom(int id, List<DateTime> dateTimes, RoomPlanType planType)
        {
            return DbSet.Where(_ => _.RoomId == id
                            && dateTimes.Contains(_.DateOcuppied)
                            && _.RoomPlanType == planType);
        }

        public void UpdateRoomPlans(List<RoomPlan> roomPlans)
        {
            DbSet.UpdateRange(roomPlans);
        }

        public void RemoveBookingRoomPlan(int id)
        {
            DbSet.RemoveRange(DbSet.Where(_ => _.BookingId == id));
        }

        public void RemoveRoomPlan(int checkinId, DateTime date)
        {
            DbSet.RemoveRange(DbSet.Where(_ => _.CheckinId == checkinId && _.DateOcuppied > date));
        }
    }
}
