﻿using HotelManagement.Domain.Entities;
using HotelManagement.Domain.RepositoryInterfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace HotelManagement.Infrastructure.Repositories
{
    public class BookingRoomRepository : Repository<BookingRoom>, IBookingRoomRepository
    {
        public BookingRoomRepository(DbFactory dbFactory) : base(dbFactory) { }
    }
}
