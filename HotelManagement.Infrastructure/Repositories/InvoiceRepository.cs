﻿using HotelManagement.Domain.Entities;
using HotelManagement.Domain.RepositoryInterfaces;
using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Text;

namespace HotelManagement.Infrastructure.Repositories
{
    public class InvoiceRepository : Repository<Invoice>, IInvoiceRepository
    {
        public InvoiceRepository(DbFactory dbFactory) : base(dbFactory)
        {

        }

        public IQueryable<Invoice> GetInvoices(InvoiceStatus status)
        {
            IQueryable<Invoice> Query = DbSet.Include(_ => _.InvoiceDetails)
                                             .ThenInclude(_ => _.DailyAudit)
                                             .ThenInclude(_ => _.RoomPlan)
                                             .ThenInclude(_ => _.Services);
            if ((int)status >= 0)
            {
                Query = Query.Where(_ => _.Status == status && _.DeletedAt == null);
            }
            return Query;
        }

        public IQueryable<Invoice> GetInvoice(int id)
        {
            return DbSet
                .Include(_ => _.DepositInvoices)
                .Include(_ => _.Checkout)
                .ThenInclude(c => c.Checkin)
                .ThenInclude(r => r.Room)
                .Where(_ => _.Id == id && _.DeletedAt == null);
        }

        public IQueryable<Invoice> GetDepositInvoice(Invoice invoice)
        {
            return DbSet.Where(_ => _.DepositInvoices.Contains(invoice));
        }
    }
}
