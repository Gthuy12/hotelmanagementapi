﻿using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Linq;
using HotelManagement.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using HotelManagement.Domain.RepositoryInterfaces;

namespace HotelManagement.Infrastructure.Repositories
{
    public class ClientRepository :Repository<Client>, IClientRepository
    {
        public ClientRepository(DbFactory dbFactory) : base(dbFactory)
        {
        }
        public Task<Client> GetClient(int clientId)
        {
            return DbSet.SingleOrDefaultAsync(t => t.Id == clientId);
        }
        public Task<List<Client>> GetClientsByIds(List<int> clientIds)
        {
            return DbSet.Where(t => clientIds.Contains(t.Id)).ToListAsync();
        }
        public Task<List<Client>> GetClients(int pageIndex, int pageSize, string search)
        {
            return DbSet
                .Where(t => t.FullName.Contains(search) || t.IdentityCode.Contains(search))
                .Skip((pageIndex - 1) * pageSize)
                .Take(pageSize)
                .ToListAsync();
        }  
    }
}
