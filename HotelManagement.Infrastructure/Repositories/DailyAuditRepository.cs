﻿using HotelManagement.Domain.Entities;
using HotelManagement.Domain.RepositoryInterfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HotelManagement.Infrastructure.Repositories
{
    public class DailyAuditRepository : Repository<DailyAudit>, IDailyAuditRepository
    {
        public DailyAuditRepository(DbFactory dbFactory) : base(dbFactory)
        {

        }
        public void AddDailyAudits(List<DailyAudit> audits)
        {
            DbSet.AddRange(audits);
        }

        public void RemoveDailyAudits(DateTime date)
        {
            DbSet.RemoveRange(DbSet.Where(_ => _.DateOcuppied.Date == date).ToList());
        }

        public void RemoveDailyAudit(DateTime date, int id)
        {
            DbSet.Remove(DbSet.Where(_ => _.DateOcuppied.Date == date && _.RoomPlan.Id == id).FirstOrDefault());
        }

        public IQueryable<DailyAudit> GetDailyAudits(List<int> roomPlanIds)
        {
            return DbSet
                .Include(_ => _.RoomPlan)
                .ThenInclude(r => r.Services)
                .Where(_ => roomPlanIds.Contains(_.RoomPlan.Id));
        }
    }
}
