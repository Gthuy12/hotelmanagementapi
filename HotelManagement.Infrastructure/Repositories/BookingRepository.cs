﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using HotelManagement.Domain.Entities;
using HotelManagement.Domain.RepositoryInterfaces;

namespace HotelManagement.Infrastructure.Repositories
{
    public class BookingRepository : Repository<Booking>, IBookingRepository
    {
        public BookingRepository(DbFactory dbFactory) : base(dbFactory) {}
        public Task<Booking> GetBookingByCode(string code)
        {
            return DbSet.SingleOrDefaultAsync(t => t.Code == code);
        }
        public Task<Booking> GetBookingById(int id)
        {
            return DbSet
                .Include(_ => _.Clients)
                .Include(_ => _.RoomQuantities)
                .SingleOrDefaultAsync(t => t.Id == id);
        }

        // Query Booking to get unavailable rooms with time
        public IQueryable<Booking> QueryBooking(DateTime checkin, DateTime checkout)
        {
            return DbSet
                .Where(_ => (_.CheckinTime >= checkin && _.CheckinTime < checkout) || (_.CheckoutTime > checkin && _.CheckoutTime <= checkout));
        }
    }
}
