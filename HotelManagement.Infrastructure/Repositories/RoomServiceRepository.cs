﻿using HotelManagement.Domain.Entities;
using HotelManagement.Domain.RepositoryInterfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HotelManagement.Infrastructure.Repositories
{
    public class RoomServiceRepository : Repository<RoomService>, IRoomServiceRepository
    {
        public RoomServiceRepository(DbFactory dbFactory) : base(dbFactory)
        {

        }
        public void AddServices(List<RoomService> roomServices)
        {
            DbSet.AddRange(roomServices);
        }

        public void RemoveRoomServices(int roomPlanId)
        {
            DbSet.RemoveRange(DbSet.Where(_ => _.RoomPlanId == roomPlanId));
        }
    }
}
