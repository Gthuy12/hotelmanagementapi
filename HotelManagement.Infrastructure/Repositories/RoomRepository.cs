﻿using System.Linq;
using System.Collections.Generic;
using HotelManagement.Domain.Entities;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using System;
using HotelManagement.Domain.RepositoryInterfaces;

namespace HotelManagement.Infrastructure.Repositories
{
    public class RoomRepository : Repository<Room>, IRoomRepository
    {
        public RoomRepository(DbFactory dbFactory) : base(dbFactory)
        {
        }
        public Task<Room> GetRoom(int id)
        {
            return DbSet.SingleOrDefaultAsync(t => t.Id == id);
        }

        public Task<List<Room>> GetRooms(int floor, RoomStatus status, string search)
        {
            if (status == RoomStatus.All)
            {
                return DbSet.Include(t => t.Bookings.Where(p => p.Status == BookingStatus.Pending)).Where(t => t.Floor == floor).ToListAsync();
            } else if (status == RoomStatus.Available && floor == 0)
            {
                return DbSet.Where(t =>
                            (t.Status == status && (search != null ? t.RoomCode.Contains(search) : true))
                            || (t.Status == status && (search != null ? t.Price.ToString().Contains(search) : true)))
                            .ToListAsync();
            }

            return DbSet.Include(t => t.Bookings.Where(p => p.Status == BookingStatus.Pending))
                    .Where(t => t.Status == status && t.Floor == floor).ToListAsync();
        }

        // Get available room to booking with checkin and checkout
        public Task<List<Room>> GetRooms(DateTime checkin, DateTime checkout, string search)
        {
            // 1. Get room list is booked on time
            //var BookingContext = DBContext.Set<Booking>();
            //var roomIds = BookingContext.Where(_ => _.CheckinTime >= checkin && _.CheckoutTime <= checkout)
            //    .SelectMany(_ => _.Rooms
            //        .Select(r => r.Id))
            //        .ToList();

            // 2. Return available rooms on time
            //return DbSet.Where(t => !roomIds.Contains(t.Id)).ToListAsync();
            return DbSet.Where(_ => _.Status != RoomStatus.Available).ToListAsync();
        } 

        // Get rooms with booking by checkin and checkout
        // Use for room plan
        public Task<List<Room>> GetRoomByDate(DateTime beginDate, DateTime endDate)
        {
            return DbSet
                .Include(_ => _.Bookings.Where(b => b.CheckinTime >= beginDate && b.CheckoutTime <= endDate))
                .ToListAsync();
        }

        public Task<List<Room>> GetUnavailableRooms()
        {
            return DbSet.Where(_ => _.Status != RoomStatus.Available).ToListAsync();
        }
    }
}
