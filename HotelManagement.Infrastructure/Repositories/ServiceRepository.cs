﻿using System.Linq;
using System.Collections.Generic;
using HotelManagement.Domain.Entities;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using HotelManagement.Domain.RepositoryInterfaces;

namespace HotelManagement.Infrastructure.Repositories
{
    public class ServiceRepository : Repository<Service>, IServiceRepository
    {
        public ServiceRepository(DbFactory dbFactory) : base(dbFactory)
        {
        }

        public Service GetService(int serviceId)
        {
            return DbSet.SingleOrDefault(_ => _.Id == serviceId);
        }

        public Task<List<Service>> GetServices(List<int> serviceIds)
        {
            return DbSet.Where(_ => serviceIds.Contains(_.Id)).ToListAsync();
        }
    }
}
