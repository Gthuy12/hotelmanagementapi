﻿using HotelManagement.Domain.Entities;
using HotelManagement.Domain.RepositoryInterfaces;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Collections.Generic;

namespace HotelManagement.Infrastructure.Repositories
{
    public class CheckinRepository : Repository<Checkin>, ICheckinRepository
    {
        public CheckinRepository(DbFactory dbFactory) : base(dbFactory)
        {

        }
        public IQueryable<Checkin> GetCheckin(int id)
        {
            return DbSet
                .Include(_ => _.Room)
                .Include(_ => _.Clients)
                .Include(_ => _.Invoices) // Check invoice is debt
                .Include(_ => _.RoomPlans)
                .Where(_ => _.Id == id);
        }

        public IQueryable<Checkin> GetCheckins()
        {
            return DbSet
                .Include(_ => _.Room)
                .Include(_ => _.Invoices);
        }

        public IQueryable<Checkin> GetDepositCheckin(Invoice invoice)
        {
            return DbSet
                .Where(_ => _.Invoices.Contains(invoice));
        }
    }
}
