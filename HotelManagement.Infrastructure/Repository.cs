﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using HotelManagement.Domain.Interfaces;
using HotelManagement.Infrastructure;

namespace HotelManagement.Infrastructure
{
    public class Repository<T> : IRepository<T> where T : class
    {
        private readonly DbFactory _dbFactory;
        private DbSet<T> _dbSet;

        public Repository(DbFactory dbFactory)
        {
            _dbFactory = dbFactory;
        }
        protected DbSet<T> DbSet
        {
            get => _dbSet ?? (_dbSet = _dbFactory.DbContext.Set<T>());
        }

        protected DbContext DBContext => _dbFactory.DbContext;

        public void SetState(T entity, EntityState state)
        {
            DBContext.Entry(entity).State = state;
        }

        public void Add(T entity)
        {
            DbSet.Add(entity);
        }
        public void Delete(T entity)
        {
            DbSet.Remove(entity);
        }
        public void Update(T entity)
        {
            DbSet.Update(entity);
        }  
        public void RemoveRange(List<T> entities)
        {
            DbSet.RemoveRange(entities);
        }

        public void AddRange(List<T> entities)
        {
            DbSet.AddRange(entities);
        }
    }
}
