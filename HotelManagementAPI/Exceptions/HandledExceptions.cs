﻿using System;
using System.Runtime.Serialization;
using Microsoft.AspNetCore.Mvc.Filters;
using HotelManagementAPI.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;

namespace HotelManagementAPI.Exceptions
{
    public class HandledExceptions : Exception, ISerializable
    {
        public int ErrorCode = 400; 
        public HandledExceptions() : base() { }
        public HandledExceptions(string message) : base(message) { }
        public HandledExceptions(string message, Exception inner) : base(message, inner) { }  
    }
    public class CustomExceptionFilter : ExceptionFilterAttribute
    {
        public override void OnException(ExceptionContext context)
        {
            if (context.Exception is HandledExceptions)
            {
                var ex = context.Exception as HandledExceptions;
                context.Exception = null;
                context.HttpContext.Response.StatusCode = ex.ErrorCode;
                context.Result = new JsonResult(new MessageModel()
                {
                    IsSuccess = false,
                    Message = ex.Message
                });
            } else if (context.Exception is NullReferenceException)
            {
                context.Exception = null;
                context.HttpContext.Response.StatusCode = StatusCodes.Status400BadRequest;
                context.Result = new JsonResult(new MessageModel()
                {
                    IsSuccess = false,
                    Message = "Bad request, please check field"
                });
            } else
            {
                context.Exception = null;
                context.HttpContext.Response.StatusCode = StatusCodes.Status500InternalServerError;
                context.Result = new JsonResult(new MessageModel()
                {
                    IsSuccess = false,
                    Message = "unhandled error occurred"
                });
            }

            base.OnException(context);
        }
    }
}
