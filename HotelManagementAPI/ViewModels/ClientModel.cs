﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HotelManagementAPI.ViewModels
{
    public class ClientViewModel
    {
        public string FullName { get; set; }
        public string Address { get; set; }
        public string Email { get; set; }
        public string IdentityCode { get; set; }
        public string PhoneNumber { get; set; }
        public string Nationality { get; set; }
        public string Notes { get; set; }
    }

    public class ClientFilter : PaginateFilter
    {
        public string Search { get; set; }
    }
}
