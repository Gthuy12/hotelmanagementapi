﻿using HotelManagement.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HotelManagementAPI.ViewModels
{
    public class RoomPlanModel
    {
        public List<ServiceModel> Services { get; set; }
    }

    public class RoomPlanFilter
    {
        public DateTime Begin { get; set; }
        public DateTime End { get; set; }
    }

    public class PreviewInvoiceViewModel
    {
        public List<DailyAudit> Audits { get; set; }
        public decimal TotalPrice { get; set; }
        public decimal RoomPrice { get; set; }
        public decimal ServicePrice { get; set; }
    }
}
