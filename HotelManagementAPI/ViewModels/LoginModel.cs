﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HotelManagementAPI.ViewModels
{
    public class LoginForm
    {
        public string username { get; set; }
        public string password { get; set; }
    }
    public class TokenModel
    {
        public string accessToken { get; set; }
    }
}
