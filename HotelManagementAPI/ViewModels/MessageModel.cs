﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HotelManagementAPI.ViewModels
{
    public class MessageModel
    {
        public bool IsSuccess { get; set; }
        public string Message { get; set; }
    }
    public class MessageModel<T> : MessageModel
    {
        public T Data { get; set; }
    }
}
