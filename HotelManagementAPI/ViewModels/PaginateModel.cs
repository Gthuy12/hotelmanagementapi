﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HotelManagementAPI.ViewModels
{
    public class PaginateFilter
    {
        public int PageIndex { get; set; }
        public int PageSize { get; set; }
        public PaginateFilter()
        {
            this.PageIndex = 1;
            this.PageSize = 5;
        }
    }

    public class PaginateDataResponse<T> : ResponseModel<T>
    {
        public int PageItem { get; set; }
        public int TotalPage { get; set; }
        public int TotalItem { get; set; }
        public int PageIndex { get; set; }
        public int PageSize { get; set; }
        public PaginateDataResponse()
        {

        }
    }
}
