﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HotelManagementAPI.ViewModels
{
    public class UserResponseData
    {
        public string Username { get; set; }
        public string Email { get; set; }
        public bool Status { get; set; }
    }
}
