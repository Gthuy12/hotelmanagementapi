﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HotelManagement.Domain.Entities;

namespace HotelManagementAPI.ViewModels
{
    public class CheckinViewModel
    {
        public DateTime Checkin { get; set; }
        public DateTime Checkout { get; set; }
        public string Notes { get; set; }
        public string ContactName { get; set; }
        public string ContactPhone { get; set; }
        public List<Client> Clients { get; set; }
        public List<Room> Rooms { get; set; } // use for group checkin
        public int RoomId { get; set; }
        public int BookingId { get; set; }
        public decimal Prepay { get; internal set; }
        public decimal Discount { get; internal set; }
    }

    public class CheckinFilter : PaginateFilter 
    {
        public DateTime Checkin { get; set; }
    }
}
