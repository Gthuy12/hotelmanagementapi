﻿using HotelManagement.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HotelManagementAPI.ViewModels
{
    public enum ActionUpdateInvoice
    {
        UpdateRoomPrice,
        UpdateServicePrice,
        UpdateDepositCheckin,
        UpdateDepositInvoice,
        PayInvoice
    }
    public class InvoiceViewModel
    {
        public InvoiceStatus Status { get; set; }
        public decimal Prepay { get; set; }
        public decimal Discount { get; set; }
        public int DepositCheckinId { get; set; }
        public Checkin Checkin { get; set; }
        public bool IsGroupCheckout { get; set; }
        public List<Invoice> Invoices { get; set; }
        public List<ServiceModel> Services { get; set; }
        public List<Checkin> Checkins { get; set; }
    }
    public class CheckInvoiceViewModel : InvoiceViewModel
    {
        public string RoomPrice { get; set; }
        public string ServicePrice { get; set; }
        public string TotalPrice { get; set; }
        public DateTime CheckinTime { get; set; }
        public DateTime CheckoutTime { get; set; }
        public Room Room { get; set; }
        public string ContactName { get; set; }
        public string ContactPhone { get; set; }
    }

    public class UpdateInvoiceViewModel : InvoiceViewModel
    {
        public DateTime CheckinTime { get; set; }
        public DateTime CheckoutTime { get; set; }
        public int DepositInvoiceId { get; set; }
        public ActionUpdateInvoice actionType { get; set; }
    }

    public class InvoiceFilter : PaginateFilter
    {
        public InvoiceStatus Status { get; set; }
    }
}
