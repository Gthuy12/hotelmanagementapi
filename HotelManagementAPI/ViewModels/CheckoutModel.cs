﻿using HotelManagement.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HotelManagementAPI.ViewModels
{
    public class CheckoutViewModel
    {
        public InvoiceStatus status { get; set; }
        public int DepositCheckinId { get; set; }
    }
    public class GroupCheckoutModel
    {
        public DateTime Checkin;
        public DateTime Checkout;
        public string Notes;
        public string ContactName;
        public string ContactEmail;
        public string BookingId;
        public List<int> Rooms;
        public List<int> Clients;
        public List<ServiceModel> Services;
    }
}
