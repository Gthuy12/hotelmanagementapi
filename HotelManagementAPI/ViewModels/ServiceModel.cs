﻿using System;
using HotelManagement.Domain.Entities;

namespace HotelManagementAPI.ViewModels
{
    public class ServiceModel
    {
        public Service Service { get; set; }
        public int Quantity { get; set; }
        public decimal Price { get; set; }
    }
    public class ServiceViewModel
    {
        public string Name { get; set; }
        public ServiceType Type { get; set; }  
        public decimal Price { get; set; }
        public string Description  { get; set; }
    }
}
