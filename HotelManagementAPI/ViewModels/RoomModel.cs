﻿using System;
using System.Collections.Generic;
using HotelManagement.Domain.Entities;

namespace HotelManagementAPI.ViewModels
{
    public class RoomFilters
    {
        public int Floor { get; set; }
        public RoomStatus Status { get; set; }
        public string Search { get; set; }
        public DateTime DateStart { get; set; }
        public DateTime DateEnd { get; set; }
        public RoomFilters()
        {
            this.Floor = 1;
            this.Status = RoomStatus.All;
        }
    }
    public class RoomResponseData
    {
        public int Floor { get; set; }
        public string Status { get; set; }
        public List<Room> Data { get; set; }
    }
    public class RoomViewModel
    {
        public string RoomCode { get; set; }
        public RoomType Type { get; set; }
        public RoomStatus Status { get; set; }
        public decimal Price { get; set; }
        public int Floor { get; set; }
    }
}
