﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HotelManagementAPI.ViewModels
{
    public class ResponseModel<T>
    {
        public bool Succeeded { get; set; }
        public string Errors { get; set; }
        public string Message { get; set; }
        public List<T> Data { get; set; }
    }
}
