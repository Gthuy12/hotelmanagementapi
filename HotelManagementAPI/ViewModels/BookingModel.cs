﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HotelManagement.Domain.Entities;

namespace HotelManagementAPI.ViewModels
{
    public class BookingQueryModel
    {
        public int RoomId { get; set; }
        public int BookingId { get; set; }
    }
    public class BookingViewModel
    { 
        public DateTime Checkin { get; set; }
        public DateTime Checkout { get; set; }
        public decimal Prepay { get; set; }
        public decimal Discount { get; set; }
        public string Notes { get; set; }
        public BookingStatus Status { get; set; }
        public bool isRoomPlan { get; set; }
        public string ContactName { get; set; }
        public string ContactPhone { get; set; }
        public List<BookingRoom> RoomQuantities { get; set; }
        public List<Room> Rooms { get; set; }
        public List<Client> Clients { get; set; }

    }
    public class BookingFilter : PaginateFilter
    {
        public int floor { get; set; }
        public BookingStatus status { get; set; }
        public string code { get; set; }
        public DateTime checkin { get; set; }
        public DateTime checkout { get; set; }
    }
 }
