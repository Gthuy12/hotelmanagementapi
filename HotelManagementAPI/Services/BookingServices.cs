﻿using HotelManagement.Domain.Entities;
using HotelManagement.Domain.Interfaces;
using HotelManagement.Infrastructure.Repositories;
using HotelManagementAPI.ViewModels;
using HotelManagementAPI.Exceptions;
using System.Threading.Tasks;
using System;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using HotelManagement.Domain.RepositoryInterfaces;

namespace HotelManagementAPI.Services
{
    public class BookingServices
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IBookingRepository _bookingRepository;
        private readonly IClientRepository _clientRepository;
        private readonly IRoomPlanRepository _roomPlanRepository;
        private readonly IBookingRoomRepository _bookingRoomRepository;

        public BookingServices(IUnitOfWork unitOfWork, IBookingRepository bookingRepository, 
            IClientRepository clientRepository, IRoomPlanRepository roomPlanRepository,
            IBookingRoomRepository bookingRoomRepository)
        {
            _unitOfWork = unitOfWork;
            _bookingRepository = bookingRepository;
            _clientRepository = clientRepository;
            _roomPlanRepository = roomPlanRepository;
            _bookingRoomRepository = bookingRoomRepository;
        }


        #region Public Method for booking

        // Booking (including personal and group)
        public async Task<MessageModel<int>> CreateReservation(BookingViewModel reservation, string userId)
        {
            // 1. Prepare booking
            var booking = GetBookingPrepared(userId, reservation);
            
            // 2. Add list client
            AddClients(booking, reservation.Clients);

            // 3. Add room quantity
            AddRoomQuantities(booking, reservation.RoomQuantities);

            // 4. If prepay -> Add Room Plan
            if(reservation.isRoomPlan)
            {
                await AddRoomPlan(reservation.Rooms, reservation.Checkin, reservation.Checkout, booking);
            }

            // 5. Add booking
            _bookingRepository.Add(booking);
            _unitOfWork.SaveChanges();
            return new MessageModel<int> { IsSuccess = true, Message = "Add new booking success", Data = booking.Id };
        }

        // Update booking information
        public async Task<MessageModel> UpdateBooking(BookingViewModel reservation, int id)
        {
            // 1. Get booking
            var booking = await _bookingRepository.GetBookingById(id);

            // 2. Modify clients
            UpdateClients(booking, reservation.Clients);

            // 3. Modify Room Quantites
            UpdateRoomQuantites(booking.RoomQuantities, booking, reservation.RoomQuantities);

            // 4. Modify RoomPlan
            await UpdateRoomPlan(id, reservation, booking);

            // 5. Set modify info
            UpdateBookingDetail(booking, reservation);
            
            _unitOfWork.SaveChanges();
            return new MessageModel<int> { IsSuccess = true, Message = "Update booking success", Data = booking.Id };
        }

       
        // Soft delete booking
        public async Task<MessageModel> DeleteBooking(int id)
        {
            var booking = await _bookingRepository.GetBookingById(id);

            booking.Status = BookingStatus.Deleted;
            booking.DeletedAt = DateTime.Now;
            _unitOfWork.SaveChanges();

            return new MessageModel { IsSuccess = true, Message = "Delete booking " + id + " success" };
        }

        public async Task<PaginateDataResponse<Booking>> GetBookings(BookingFilter filter)
        {
            var bookings = await _bookingRepository
                .QueryBooking(filter.checkin, filter.checkout)
                .Skip((filter.PageIndex - 1) * filter.PageSize)
                .Take(filter.PageSize)
                .ToListAsync();

            return new PaginateDataResponse<Booking>
            {
                Data = bookings,
                PageIndex = filter.PageIndex,
                PageSize = filter.PageSize,
                Succeeded = true
            };
        }

        public async Task<Booking> GetBooking(int id)
        {
            var booking = await _bookingRepository.GetBookingById(id);
            return booking;
        }
        #endregion


        #region Private Method for booking

        // Prepare new booking when create
        private Booking GetBookingPrepared(string userId, BookingViewModel reservation)
        {
            var booking = new Booking();
            booking.CreatedByAuthor = userId;
            booking.Code = BookingCode();
            booking.CreatedAt = DateTime.Now;

            SetBooking(booking, reservation);
            return booking;
        }

        // Update clients
        private void UpdateClients(Booking booking, List<Client> clients)
        {
            List<int> existIds = new List<int>();
            foreach (var client in booking.Clients)
            {
                if(clients.Select(c => c.Id).ToList().Contains(client.Id))
                {
                    existIds.Add(client.Id);
                    _clientRepository.SetState(client, EntityState.Detached);
                }
            }

            booking.Clients.Clear();
            
            foreach (var client in clients)
            {
                if (client.Id == 0)
                {
                    _clientRepository.Add(client);
                } else
                {
                    _clientRepository.SetState(client, EntityState.Modified);
                }
                // If belong list exist dont add to list
                if(!existIds.Contains(client.Id))
                {
                    booking.Clients.Add(client);
                } 
            }
        }
        private void UpdateBookingDetail(Booking booking, BookingViewModel reservation)
        {
            SetBooking(booking, reservation);
            booking.UpdatedAt = DateTime.Now;
            _bookingRepository.Update(booking);
        }

        private async Task UpdateRoomPlan(int id, BookingViewModel reservation, Booking booking)
        {
            _roomPlanRepository.RemoveBookingRoomPlan(id);
            if (reservation.isRoomPlan && reservation.Rooms.Count > 0)
            {
                await AddRoomPlan(reservation.Rooms, reservation.Checkin, reservation.Checkout, booking);
            }
        }

        private void UpdateRoomQuantites(ICollection<BookingRoom> OldRoomQuantities, Booking booking, List<BookingRoom> NewRoomQuantities)
        {
            // Remove old room quantity
            _bookingRoomRepository.RemoveRange(OldRoomQuantities.ToList());

            // Add new room quantity
            AddRoomQuantities(booking, NewRoomQuantities);
        }


        // Add room quantity to booking
        private void AddRoomQuantities(Booking booking, List<BookingRoom> roomQuantities)
        {
            foreach (var room in roomQuantities)
            {
                booking.RoomQuantities.Add(room);
            }
        }

        // Add room plan if booking with room plan
        private async Task AddRoomPlan(List<Room> rooms, DateTime checkinTime, DateTime checkoutTime, Booking booking)
        {
            var dates = GetDateArrange(checkinTime, checkoutTime);
            var unvailableRooms = await _roomPlanRepository.GetRooms(rooms, dates, RoomPlanType.Booking).ToListAsync();
            if (unvailableRooms.Count > 0)
            {
                throw new HandledExceptions("Rooms will be occupied on that day");
            }

            var roomPlans = new List<RoomPlan>();
            foreach (var room in rooms)
            {
                foreach (var date in dates)
                {
                    var roomPlan = new RoomPlan
                    {
                        RoomId = room.Id,
                        DateOcuppied = date,
                        Booking = booking
                    };
                    roomPlans.Add(roomPlan);
                }
            }
            _roomPlanRepository.AddRange(roomPlans);
        }

        // Get date array to create room plan
        private List<DateTime> GetDateArrange(DateTime checkinTime, DateTime checkoutTime)
        {
            var dates = new List<DateTime>();
            for (var dt = checkinTime; dt <= checkoutTime; dt = dt.AddDays(1))
            {
                dates.Add(dt);
            }

            return dates;
        }

        // Add clients when create booking
        private void AddClients(Booking booking, List<Client> clients)
        {           
            foreach (var client in clients)
            {
                if (client.Id == 0)
                {
                    _clientRepository.Add(client);
                }
                else
                {
                    _clientRepository.SetState(client, EntityState.Modified);
                }
                booking.Clients.Add(client);
            }
        }

        // Set booking information
        private void SetBooking(Booking booking, BookingViewModel bookingVM)
        {
            booking.CheckinTime = bookingVM.Checkin;
            booking.CheckoutTime = bookingVM.Checkout;
            booking.Prepay = bookingVM.Prepay;
            booking.Discount = bookingVM.Discount;
            booking.Status = bookingVM.Status;
            booking.Notes = bookingVM.Notes;
            booking.ContactName = bookingVM.ContactName;
            booking.ContactPhone = bookingVM.ContactPhone;
        }

        // Generate booking code
        private string BookingCode()
        {
            var random = new Random();
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            
            return new string(Enumerable.Repeat(chars, 8)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }
        #endregion
    }
}