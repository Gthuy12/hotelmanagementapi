﻿using HotelManagement.Domain.Entities;
using HotelManagement.Domain.Interfaces;
using HotelManagement.Domain.RepositoryInterfaces;
using HotelManagementAPI.ViewModels;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HotelManagementAPI.Services
{
    public class RoomPlanServices
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly ICheckinRepository _checkinRepository;
        private readonly IRoomPlanRepository _roomPlanRepository;
        private readonly IRoomServiceRepository _roomServiceRepository;
        private readonly IDailyAuditRepository _dailyAuditRepository;

        public RoomPlanServices(IUnitOfWork unitOfWork, ICheckinRepository checkinRepository,
            IRoomPlanRepository roomPlanRepository, IRoomServiceRepository roomServiceRepository, 
            IDailyAuditRepository dailyAuditRepository)
        {
            _unitOfWork = unitOfWork;
            _checkinRepository = checkinRepository;
            _roomPlanRepository = roomPlanRepository;
            _roomServiceRepository = roomServiceRepository;
            _dailyAuditRepository = dailyAuditRepository;
        }

        #region Public method for RoomPlan
        // Retrive room plans
        public async Task<List<RoomPlan>> GetRoomPlans(RoomPlanFilter roomPlanFilter)
        {
            var roomPlans = await _roomPlanRepository
                .GetRoomPlans()
                .Where(_ => _.DateOcuppied >= roomPlanFilter.Begin
                            && _.DateOcuppied <= roomPlanFilter.End)
                .OrderBy(_ => _.RoomId)
                .ToListAsync();

            return roomPlans;
        }

        // Change room feature - add logic here
        public async Task<MessageModel> ChangeRoomForCheckin(CheckinViewModel checkinVM, int checkinId, int roomId)
        {
            // 1. Get checkin
            var checkin = await _checkinRepository.GetCheckin(checkinId).FirstOrDefaultAsync();

            // 2. Clear old room plan
            _roomPlanRepository.RemoveRoomPlan(checkinId, DateTime.Now);

            // 3. Add new room plan with new room
            AddRoomPlan(roomId, DateTime.Now, checkinVM.Checkout, checkin);

            // 4. Commit transaction
            _unitOfWork.SaveChanges();
            return new MessageModel { IsSuccess = true, Message = "Change room success" };
        }

        // Update room plan
        public async Task<MessageModel> UpdateRoomPlan(CheckinViewModel checkinVM, int checkinId)
        {
            // 1. Get checjkin
            var checkin = await _checkinRepository.GetCheckin(checkinId).FirstOrDefaultAsync();

            // 2. Clear old room plan (from this day)
            _roomPlanRepository.RemoveRoomPlan(checkinId, DateTime.Now);

            // 3. Add new room plan
            AddRoomPlan(checkin.RoomId, DateTime.Now, checkinVM.Checkout, checkin);

            // 4. commit transaction            
            _unitOfWork.SaveChanges();
            return new MessageModel { IsSuccess = true, Message = "Update checkin success" };
        }

        // Update room plan services each day
        public async Task<MessageModel> UpdateRoomServices(RoomPlanModel roomPlanVM, int roomPlanId)
        {
            // 1. Get room plan
            _roomServiceRepository.RemoveRoomServices(roomPlanId);

            // 2. Add services 
            AddRoomServices(roomPlanVM, roomPlanId);

            // 3. Commit transaction
            _unitOfWork.SaveChanges();
            return new MessageModel { IsSuccess = true, Message = "Update checkin success" };
        }

        // Daily audit for all system
        public async Task<MessageModel> DailyAudit()
        {
            // Get room plan on day
            var roomPlans = await _roomPlanRepository
                .GetRoomPlans()
                .Include(_ => _.Room)
                .Include(_ => _.Services)
                .Where(_ => _.RoomPlanType == RoomPlanType.Checkin
                            && _.DateOcuppied.Day == DateTime.Now.Day)
                .ToListAsync();

            // Add daily audit and change room plan status to Done
            AddRangeAudit(roomPlans);

            // Commit transaction
            _unitOfWork.SaveChanges();
            return new MessageModel { IsSuccess = true, Message = "Create audit for today success" };
        }

        public async Task<MessageModel> DailyAuditCheckin(int id, DateTime date)
        {
            var roomPlan = await _roomPlanRepository
               .GetRoomPlans()
               .Include(_ => _.Room)
               .Include(_ => _.Services)
               .Where(_ => _.RoomPlanType == RoomPlanType.Checkin
                           && _.DateOcuppied.Day == date.Day
                           && _.CheckinId == id)
               .FirstOrDefaultAsync();

            // Add Audit for room plan
            AddAudit(roomPlan, date);

            _unitOfWork.SaveChanges();
            return new MessageModel { IsSuccess = true, Message = "Create audit for today success" };
        }

        public async Task<PreviewInvoiceViewModel> GetPreviewInvoice(int id)
        {
            var roomPlans = await _roomPlanRepository
                .GetRoomPlans()
                .Where(_ => _.CheckinId == id && _.RoomPlanStatus == RoomPlanStatus.Done)
                .Select(_ => _.Id)
                .ToListAsync();

            var audits = await _dailyAuditRepository.GetDailyAudits(roomPlans).ToListAsync();

            var preview = new PreviewInvoiceViewModel
            {
                Audits = audits,
                TotalPrice = audits.Sum(_ => _.TotalPrice),
                RoomPrice = audits.Sum(_ => _.RoomPrice),
                ServicePrice = audits.Sum(_ => _.ServicePrice)
            };

            return preview;
        }
        #endregion


        #region Private method for RoomPlan
        private void AddAudit(RoomPlan roomPlan, DateTime date)
        {
            _dailyAuditRepository.RemoveDailyAudit(date, roomPlan.Id);

            var audit = new DailyAudit
            {
                DateOcuppied = roomPlan.DateOcuppied,
                RoomPlan = roomPlan,
                ServicePrice = roomPlan.Services.Sum(_ => _.Price),
                RoomPrice = roomPlan.Room.Price,
                TotalPrice = roomPlan.Services.Sum(_ => _.Price) + roomPlan.Room.Price, // Create trigger here
            };
            roomPlan.RoomPlanStatus = RoomPlanStatus.Done;

            _roomPlanRepository.Update(roomPlan);
            _dailyAuditRepository.Add(audit);
        }

        private void AddRangeAudit(List<RoomPlan> roomPlans)
        {
            _dailyAuditRepository.RemoveDailyAudits(DateTime.Now.Date);

            var audits = new List<DailyAudit>();
            foreach (var roomPlan in roomPlans)
            {
                audits.Add(new DailyAudit
                {
                    DateOcuppied = roomPlan.DateOcuppied,
                    RoomPlan = roomPlan,
                    ServicePrice = roomPlan.Services.Sum(_ => _.Price),
                    RoomPrice = roomPlan.Room.Price,
                    TotalPrice = roomPlan.Services.Sum(_ => _.Price) + roomPlan.Room.Price, // Create trigger here
                });
                roomPlan.RoomPlanStatus = RoomPlanStatus.Done;
            }

            // Clear room occupied
            _roomPlanRepository.UpdateRoomPlans(roomPlans);
            _dailyAuditRepository.AddRange(audits);
        }

        private void AddRoomServices(RoomPlanModel roomPlanVM, int roomPlanId)
        {
            List<RoomService> roomServices = new List<RoomService>();
            foreach (var service in roomPlanVM.Services)
            {
                roomServices.Add(new RoomService
                {
                    RoomPlanId = roomPlanId,
                    ServiceId = service.Service.Id,
                    Quantity = service.Quantity,
                    Price = service.Service.Price * service.Quantity
                });
            }

            _roomServiceRepository.AddRange(roomServices);
        }

        private void AddRoomPlan(int roomId, DateTime beginDate, DateTime checkoutDate, Checkin checkin)
        {
            checkin.CheckoutTime = checkoutDate;
            _checkinRepository.Update(checkin);

            var dates = GetDateArrange(beginDate, checkoutDate);

            var roomPlans = new List<RoomPlan>();
            foreach (var date in dates)
            {
                var roomPlan = new RoomPlan
                {
                    RoomId = roomId,
                    DateOcuppied = date,
                    Checkin = checkin
                };
                roomPlans.Add(roomPlan);
            }
            _roomPlanRepository.AddRange(roomPlans);
        }

        private List<DateTime> GetDateArrange(DateTime checkinTime, DateTime checkoutTime)
        {
            var dates = new List<DateTime>();
            for (var dt = checkinTime; dt <= checkoutTime; dt = dt.AddDays(1))
            {
                dates.Add(dt);
            }

            return dates;
        }
        #endregion
    }
}
