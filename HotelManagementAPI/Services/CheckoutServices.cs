﻿using HotelManagement.Domain.Entities;
using HotelManagement.Domain.Interfaces;
using HotelManagement.Domain.RepositoryInterfaces;
using HotelManagementAPI.Exceptions;
using HotelManagementAPI.ViewModels;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HotelManagementAPI.Services
{
    public class CheckoutServices
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly ICheckinRepository _checkinRepository;
        private readonly ICheckoutRepository _checkoutRepository;
        private readonly IInvoiceRepository _invoiceRepository;
        private readonly IRoomPlanRepository _roomPlanRepository;
        private readonly IDailyAuditRepository _dailyAuditRepository;

        public CheckoutServices(ICheckinRepository checkinRepository, ICheckoutRepository checkoutRepository,
            IInvoiceRepository invoiceRepository, IUnitOfWork unitOfWork, IRoomPlanRepository roomPlanRepository,
            IDailyAuditRepository dailyAuditRepository)    
        {
            _unitOfWork = unitOfWork;
            _checkinRepository = checkinRepository;
            _checkoutRepository = checkoutRepository;
            _invoiceRepository = invoiceRepository;
            _roomPlanRepository = roomPlanRepository;
            _dailyAuditRepository = dailyAuditRepository;
        }

        #region Public method for checkout
        // Checkout personal
        public async Task<MessageModel> CheckoutPersonal(string createdById, int checkinId, CheckoutViewModel checkoutVM)
        {
            // Get checkin
            var checkin = await _checkinRepository.GetCheckin(checkinId).SingleOrDefaultAsync();

            // Checkout, change checkin status
            var checkout = CreateCheckout(checkin, createdById);

            // Create invoice
            var invoice = new Invoice();
            SetInvoiceInfomation(createdById, checkout, invoice, checkoutVM.status);
            AddInvoice(checkin, invoice);

            // Add to checkin if deposit 
            if(checkoutVM.status == InvoiceStatus.Deposit)
            {
                await SaveInvoiceDeposit(invoice, checkoutVM.DepositCheckinId);
            }

            //Commit transaction
            _unitOfWork.SaveChanges();
            return new MessageModel { IsSuccess = true, Message = "Checkout personal success" };
        }

        // Checkout group
        public async Task<MessageModel> CheckoutGroup(List<int> checkinIds, string authorId, CheckoutViewModel checkoutVM)
        {
            // Get checkins
            List<Checkin> checkins = await _checkinRepository
                .GetCheckins()
                .Where(_ => checkinIds.Contains(_.Id))
                .ToListAsync();

            // Checkout and change checkin status
            var checkouts = CreateCheckouts(checkins, authorId);

            // Create invoices
            var invoices = AddInvoices(checkins, checkouts, checkoutVM);

            // Add to checkin if deposit
            if (checkoutVM.status == InvoiceStatus.Deposit)
            {
                await SaveInvoiceDeposits(invoices, checkoutVM.DepositCheckinId);
            }

            //Commit transaction
            _unitOfWork.SaveChanges();
            return new MessageModel { IsSuccess = true, Message = "Checkout success" };
        }
        #endregion

        #region Private method for personal checkout

        // Create checkouts (personal checkout)
        private Checkout CreateCheckout(Checkin checkin, string createdById)
        {
            // Checkin done
            ValidateCheckin(checkin); // Add condition when create checkin
            checkin.Status = CheckinStatus.Done;
            _checkinRepository.Update(checkin);

            // Remove room plan tomorrow
            _roomPlanRepository.RemoveRoomPlan(checkin.Id, DateTime.Now.AddDays(1));

            // Add checkout
            var checkout = new Checkout
            {
                CheckinId = checkin.Id,
                CheckinTime = checkin.CheckinTime,
                CheckoutTime = DateTime.Now,
                AuthorId = createdById
            };
            _checkoutRepository.Add(checkout);

            return checkout;
        }
        // Save 1 invoice to checkin (personal checkout)
        private async Task SaveInvoiceDeposit(Invoice invoice, int checkinId)
        {
            var checkin = await _checkinRepository.GetCheckin(checkinId).SingleOrDefaultAsync();
            ValidateCheckin(checkin);
            checkin.Invoices.Add(invoice);
            _checkinRepository.Update(checkin);
        }
        #endregion

        #region Private method for group checkout
        // Create checkouts (group checkout)
        private List<Checkout> CreateCheckouts(List<Checkin> checkins, string createdById)
        {
            var checkouts = new List<Checkout>();
            foreach (var checkin in checkins)
            {
                ValidateCheckin(checkin); // Add condition when create checkin
                checkin.Status = CheckinStatus.Done;
                _checkinRepository.Update(checkin);

                // Remove room plan
                _roomPlanRepository.RemoveRoomPlan(checkin.Id, DateTime.Now.AddDays(1));

                // Add checkout to list
                var checkout = new Checkout
                {
                    CheckinId = checkin.Id,
                    CheckinTime = checkin.CheckinTime,
                    CheckoutTime = DateTime.Now,
                    AuthorId = createdById
                };
                checkouts.Add(checkout);
            }
            
            // Addrange checkouts
            _checkoutRepository.AddRange(checkouts);
            return checkouts;
        }

        // Create invoices (group checkout)
        private List<Invoice> AddInvoices(List<Checkin> checkins, List<Checkout> checkouts, CheckoutViewModel checkoutVM)
        {
            var invoices = new List<Invoice>();
            for (int i = 0; i < checkouts.Count; i++)
            {
                var invoice = new Invoice();
                SetInvoiceInfomation(checkouts[i].AuthorId, checkouts[i], invoice, checkoutVM.status);
                AddInvoice(checkins[i], invoice);
                invoices.Add(invoice);
            }

            //Add range invoices
            _invoiceRepository.AddRange(invoices);
            return invoices;
        }

        // Save invoices to checkin (group checkout)
        private async Task SaveInvoiceDeposits(List<Invoice> invoices, int depositCheckinId)
        {
            var checkin = await _checkinRepository.GetCheckin(depositCheckinId).SingleOrDefaultAsync();
            ValidateCheckin(checkin);
            foreach (var invoice in invoices)
            {
                checkin.Invoices.Add(invoice);
            }

            _checkinRepository.Update(checkin);
        }
        #endregion

        #region Common private method
        // Add invoice
        private void AddInvoice(Checkin checkin, Invoice invoice)
        {
            // Get all audit from checkin.RoomPlans
            var audits = _dailyAuditRepository.GetDailyAudits(checkin.RoomPlans.Select(_ => _.Id).ToList());

            // Create invoice detail from audits
            foreach (var audit in audits)
            {
                var invoiceDetail = CreateInvoiceDetail(audit, invoice);
                invoice.InvoiceDetails.Add(invoiceDetail);
            }

            // Calculate invoice with prepay and discount
            invoice.TotalPrice = invoice.InvoiceDetails.Sum(_ => _.TotalPrice);
            invoice.ServicePrice = invoice.InvoiceDetails.Sum(_ => _.ServicePrice);
            invoice.RoomPrice = invoice.InvoiceDetails.Sum(_ => _.RoomPrice);

            // Add invoice deposit
            AddDepositInvoices(invoice, checkin.Invoices);

            _invoiceRepository.Add(invoice);
        }

        // Add deposit invoice if checkin have deposit invoice
        private void AddDepositInvoices(Invoice invoice, ICollection<Invoice> invoices)
        {
            if (invoices.Count > 0)
            {
                foreach (var invoiceItem in invoices)
                {
                    invoice.DepositInvoices.Add(invoiceItem);
                }
            }
        }

        // Validate checkin is pending
        private void ValidateCheckin(Checkin checkin)
        {
            if (checkin == null)
            {
                throw new HandledExceptions("Checkin can not be found");
            }
            if (checkin.Status != CheckinStatus.Pending)
            {
                throw new HandledExceptions("Checkin already done and deleted");
            }
        }

        // Create invoice detail
        private InvoiceDetail CreateInvoiceDetail(DailyAudit audit, Invoice invoice)
        {
            var invoiceDetail = new InvoiceDetail
            {
                Invoice = invoice,
                DailyAudit = audit,
                RoomPrice = audit.RoomPrice,
                ServicePrice = audit.ServicePrice,
                TotalPrice = audit.TotalPrice,
            };

            return invoiceDetail;
        }

        // Generate invoice code
        private string InvoiceCode()
        {
            Random random = new Random();
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

            return new string(Enumerable.Repeat(chars, 8)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }

        // Prepare invoice
        private void SetInvoiceInfomation(string authorId, Checkout checkout, Invoice invoice, InvoiceStatus status)
        {
            invoice.Code = InvoiceCode();
            invoice.Status = status; // deposit or not
            invoice.Checkout = checkout;
            invoice.AuthorId = authorId;
            invoice.CreatedAt = DateTime.Now;
        }
        #endregion
    }
}
