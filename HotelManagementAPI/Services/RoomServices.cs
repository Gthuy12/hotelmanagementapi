﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HotelManagement.Domain.Entities;
using Microsoft.Extensions.Configuration;
using HotelManagement.Domain.Interfaces;
using HotelManagement.Infrastructure.Repositories;
using HotelManagementAPI.ViewModels;
using HotelManagement.Domain.RepositoryInterfaces;

namespace HotelManagementAPI.Services
{
    public class RoomServices
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IRoomRepository _roomRepository;
        public RoomServices(IUnitOfWork unitOfWork, IRoomRepository roomRepository)
        {
            _unitOfWork = unitOfWork;
            _roomRepository = roomRepository;
        }
        public async Task<MessageModel> Add(RoomViewModel roomVM)
        {         
            var room = CreateNewRoom(roomVM);
            _roomRepository.Add(room);
            _unitOfWork.SaveChanges();

            return new MessageModel { IsSuccess = true , Message = "Create new room success" };
        }
        public async Task<MessageModel> Update(int roomId, RoomViewModel roomVM)
        {
            var currentRoom = await _roomRepository.GetRoom(roomId );

            currentRoom.RoomCode = roomVM.RoomCode;
            currentRoom.Type = roomVM.Type;
            currentRoom.Status = roomVM.Status;
            currentRoom.Price = roomVM.Price;
            currentRoom.Floor = roomVM.Floor;

            _roomRepository.Update(currentRoom);
            _unitOfWork.SaveChanges();

            return new MessageModel { IsSuccess = true, Message = "Update room success" };
        }
        public async Task<Room> GetRoom(int id)
        {
            return await _roomRepository.GetRoom(id);
        }

        public async Task<RoomResponseData> GetRooms(RoomFilters roomFilter)
        {
            var rooms = await _roomRepository.GetRooms(roomFilter.Floor, roomFilter.Status, roomFilter.Search);

            return new RoomResponseData
            {
                Data = rooms,
                Floor = roomFilter.Floor,
                Status = roomFilter.Status.ToString()
            };
        }

        public async Task<RoomResponseData> GetRoomAvaibleDate(RoomFilters roomFilter)
        {
            var rooms = await _roomRepository.GetRoomByDate(roomFilter.DateStart, roomFilter.DateEnd);

            return new RoomResponseData
            {
                Data = rooms,
                Floor = roomFilter.Floor,
                Status = roomFilter.Status.ToString()
            };
        }

        private Room CreateNewRoom(RoomViewModel roomVM)
        {
            var room = new Room
            {
                RoomCode = roomVM.RoomCode,
                Type = roomVM.Type,
                Status = roomVM.Status,
                Price = roomVM.Price,
                Floor = roomVM.Floor,
            };
             
            return room;
        }
    }
}
