﻿using HotelManagement.Domain.Entities;
using HotelManagement.Domain.RepositoryInterfaces;
using HotelManagementAPI.ViewModels;
using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;
using HotelManagement.Domain.Interfaces;
using HotelManagementAPI.Exceptions;

namespace HotelManagementAPI.Services
{
    public class InvoiceServices
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IInvoiceRepository _invoiceRepository;
        public InvoiceServices(IUnitOfWork unitOfWork ,IInvoiceRepository invoiceRepository)
        {
            _unitOfWork = unitOfWork;
            _invoiceRepository = invoiceRepository;
        }

        public async Task<PaginateDataResponse<Invoice>> GetInvoices(InvoiceFilter filter)
        {
            var invoices = await _invoiceRepository.GetInvoices(filter.Status).ToListAsync();

            return new PaginateDataResponse<Invoice>
            {
                Data = invoices,
                PageIndex = filter.PageIndex,
                PageSize = filter.PageSize
            };
        }

        // Get detail invoices
        public async Task<Invoice> GetInvoice(int id)
        {
            var invoice = await _invoiceRepository.GetInvoice(id).SingleOrDefaultAsync();

            return invoice;
        }

        public async Task<MessageModel> SetInvoiceToDone(int id)
        {
            var invoice = await GetInvoice(id);

            UpdateInvoice(invoice);

            return new MessageModel
            {
                IsSuccess = true,
                Message = "Delete invoice success",
            };
        }

        private void UpdateInvoice(Invoice invoice)
        {
            if (invoice == null)
            {
                throw new HandledExceptions("Invoice does not exist");
            } else if (invoice.Status == InvoiceStatus.Done)
            {
                throw new HandledExceptions("Invoice has been paid");
            }
            invoice.Status = InvoiceStatus.Done;

            _invoiceRepository.Update(invoice);
            _unitOfWork.SaveChanges();
        }

        public async Task<MessageModel> DeleteInvoice(int id)
        {
            // Delete invoice details
            var invoice = await GetInvoice(id);

            ClearInvoice(invoice);

            return new MessageModel
            {
                IsSuccess = true,
                Message = "Delete invoice success",
            };
        }

        private void ClearInvoice(Invoice invoice)
        {
            if (invoice == null)
            {
                throw new HandledExceptions("Invoice does not exist");
            }
            invoice.DeletedAt = DateTime.Now;

            _invoiceRepository.Update(invoice);
            _unitOfWork.SaveChanges();
        }
    }
}
