﻿using HotelManagement.Domain.Entities;
using HotelManagement.Domain.Interfaces;
using HotelManagement.Domain.RepositoryInterfaces;
using HotelManagementAPI.ViewModels;

namespace HotelManagementAPI.Services
{
    public class ServiceServices
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IServiceRepository _serviceRepository;
        public ServiceServices(IUnitOfWork unitOfWork, IServiceRepository serviceRepository)
        {
            _unitOfWork = unitOfWork;
            _serviceRepository = serviceRepository;
        }
        public MessageModel Add(ServiceViewModel serviceVM)
        {
            var service = new Service 
            { 
                Type = serviceVM.Type, 
                Name = serviceVM.Name, 
                Description = serviceVM.Description, 
                Price = serviceVM.Price 
            };
            _serviceRepository.Add(service);
            _unitOfWork.SaveChanges();

            return new MessageModel { IsSuccess = true, Message = "Create new service success" };
        }
        public MessageModel Update(int serviceId, ServiceViewModel serviceVM)
        {
            var currentService = _serviceRepository.GetService(serviceId);

            currentService.Type = serviceVM.Type;
            currentService.Name = serviceVM.Name;
            currentService.Description = serviceVM.Description;
            currentService.Price = serviceVM.Price;

            _serviceRepository.Update(currentService);
            _unitOfWork.SaveChanges();

            return new MessageModel { IsSuccess = true, Message = "Update service success" };
        }
        public Service GetService(int id)
        {
            return _serviceRepository.GetService(id);
        }
    }
}
