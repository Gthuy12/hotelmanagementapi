﻿using HotelManagement.Domain.Entities;
using HotelManagement.Domain.Interfaces;
using HotelManagement.Domain.RepositoryInterfaces;
using HotelManagementAPI.Exceptions;
using HotelManagementAPI.ViewModels;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HotelManagementAPI.Services
{
    public class CheckinServices
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly ICheckinRepository _checkinRepository;
        private readonly IClientRepository _clientRepository;
        private readonly IBookingRepository _bookingRepository;
        private readonly IRoomPlanRepository _roomPlanRepository;

        public CheckinServices(IUnitOfWork unitOfWork, ICheckinRepository checkinRepository,
            IClientRepository clientRepository, IBookingRepository bookingRepository, IRoomPlanRepository roomPlanRepository)
        {
            _unitOfWork = unitOfWork;
            _checkinRepository = checkinRepository;
            _clientRepository = clientRepository;
            _bookingRepository = bookingRepository;
            _roomPlanRepository = roomPlanRepository;
        }

        #region Public method for checkin feature
        // Checkin personal ( including checkin from reservation and walking checkin )
        public async Task<int> CheckinPersonal(CheckinViewModel checkinVM, string checkinByUserId)
        {
            // 1. Get available room
            await GetRoomPlan(checkinVM);
            
            // 2. Add checkin
            var checkin = AddCheckin(checkinVM, checkinByUserId);

            // 3. Clear booking
            await ClearBooking(checkinVM.BookingId);

            _unitOfWork.SaveChanges();
            return checkin.Id;
        }


        // Checkin group ( including checkin from reservation and walking checkin )
        public async Task<MessageModel> CheckinGroup(CheckinViewModel checkinVM, string authorId)
        {
            // 1. Get available room
            await GetRoomPlans(checkinVM);
           
            // 2. Checkin (add checkin and room plan)
            AddRangeCheckin(checkinVM, authorId);

            // 3. Change booking status
            await ClearBooking(checkinVM.BookingId);
        
            // 4. Commit transaction
            _unitOfWork.SaveChanges();
            return new MessageModel { IsSuccess = true, Message = "Create checkin success" };
        }

       
        public async Task<MessageModel> UpdateCheckin(CheckinViewModel checkinVM, int checkinId)
        {
            // 1. Get checkin
            var checkin = await _checkinRepository.GetCheckin(checkinId).FirstOrDefaultAsync();

            // 2. Update checkin
            ModifyCheckinDetail(checkin, checkinVM);

            // 3. Commit transaction
            _checkinRepository.Update(checkin);
            _unitOfWork.SaveChanges();
            return new MessageModel { IsSuccess = true, Message = "Update checkin success" };
        }

        // Get checkins
        public async Task<PaginateDataResponse<Checkin>> GetCheckins(CheckinFilter filter)
        {
            var checkins = await _checkinRepository
                .GetCheckins()
                .Where(_ => _.CheckinTime >= filter.Checkin)
                .Skip((filter.PageIndex - 1) * filter.PageSize)
                .Take(filter.PageSize)
                .ToListAsync();

            return new PaginateDataResponse<Checkin>
            {
                Data = checkins,
                PageIndex = filter.PageIndex,
                PageSize = filter.PageSize,
                Succeeded = true
            };
        }

        // Get checkin detail
        public async Task<Checkin> GetCheckin(int id)
        {
            var checkin = await _checkinRepository.GetCheckin(id).FirstOrDefaultAsync();

            return checkin;
        }
        #endregion


        #region private method for personal checkin
        private async Task GetRoomPlan(CheckinViewModel checkinVM)
        {
            var roomPlan = await _roomPlanRepository
                .GetRoom(checkinVM.RoomId, GetDateArrange(checkinVM.Checkin, checkinVM.Checkout)
                    , RoomPlanType.Checkin)
                .ToListAsync();
            if (roomPlan.Count > 0)
            {
                throw new HandledExceptions("Room unvailable with date range");
            }
        }


        // Add single checkin for personal checkin
        private Checkin AddCheckin(CheckinViewModel checkinVM, string userId)
        {
            // 1. Prepare Checkin
            // 2. Add Clients and Room Plan
            // 3. Add Checkin

            var checkin = PrepareCheckin(checkinVM, userId);

            AddClients(checkin, checkinVM.Clients);
            AddRoomPlan(checkinVM.RoomId, checkinVM.Checkin, checkinVM.Checkout, checkin);
            _checkinRepository.Add(checkin);

            return checkin;
        }
        #endregion

        #region private method for group checkin
        private async Task GetRoomPlans(CheckinViewModel checkinVM)
        {
            var roomPlan = await _roomPlanRepository
               .GetRooms(checkinVM.Rooms, GetDateArrange(checkinVM.Checkin, checkinVM.Checkout)
                   , RoomPlanType.Checkin)
               .ToListAsync();
            if (roomPlan.Count > 0)
            {
                throw new HandledExceptions("Rooms unvailable with date range");
            }
        }

        // Add checkins for group checkin
        private void AddRangeCheckin(CheckinViewModel checkinVM, string authorId)
        {
            // 1. Prepare Checkins
            // 2. Create checkin for each room, add checkin to list
            // 3. AddRange Checkins

            List<Checkin> checkins = new List<Checkin>();
            foreach (var room in checkinVM.Rooms)
            {
                checkinVM.RoomId = room.Id;
                var checkin = PrepareCheckin(checkinVM, authorId);
                checkins.Add(checkin);
                AddRoomPlan(room.Id, checkinVM.Checkin, checkinVM.Checkout, checkin);
            }

            _checkinRepository.AddRange(checkins);
        }
        #endregion

        #region common private method
        // Set checkin detail information for update checkin
        private void ModifyCheckinDetail(Checkin checkin, CheckinViewModel checkinVM)
        {
            // Update checkin infor
            checkin.ContactName = checkinVM.ContactName;
            checkin.ContactPhone = checkinVM.ContactPhone;
            checkin.Prepay = checkinVM.Prepay;
            checkin.Discount = checkinVM.Discount;

            // Update clients
            UpdateClients(checkin, checkinVM.Clients);
        }

        // Prepare checkin object
        private Checkin PrepareCheckin(CheckinViewModel checkinVM, string authorId)
        {
            var checkin = new Checkin
            {
                CheckinTime = checkinVM.Checkin != null ? checkinVM.Checkin : DateTime.Now,
                CheckoutTime = checkinVM.Checkout,
                ContactName = checkinVM.ContactName,
                ContactPhone = checkinVM.ContactPhone,
                RoomId = checkinVM.RoomId,
                CreatedByAuthor = authorId
            };

            return checkin;
        }

        // Add clients for create new checkin
        private void AddClients(Checkin checkin, List<Client> clients)
        {
            foreach (var client in clients)
            {
                if (client.Id <= 0)
                {
                    _clientRepository.Add(client);
                }
                else
                {
                    _clientRepository.SetState(client, EntityState.Modified);
                }
                checkin.Clients.Add(client);
            }
        }

        // Update clients for checkin
        private void UpdateClients(Checkin checkin, List<Client> clients)
        { 
            List<int> existIds = new List<int>();
            foreach (var client in checkin.Clients)
            {
                if (clients.Select(c => c.Id).ToList().Contains(client.Id))
                {
                    existIds.Add(client.Id);
                    _clientRepository.SetState(client, EntityState.Detached);
                }
            }

            checkin.Clients.Clear();

            foreach (var client in clients)
            {
                if (client.Id == 0)
                {
                    _clientRepository.Add(client);
                }
                else
                {
                    _clientRepository.SetState(client, EntityState.Modified);
                }

                if (!existIds.Contains(client.Id))
                {
                    checkin.Clients.Add(client);
                }
            }
        }

        // Validate booking is pending
        private void ValidateBookingStatus(Booking booking)
        {
            // Validate booking is valid
            if (booking == null)
                throw new HandledExceptions("Booking can not be found");
            else if (booking.Status != BookingStatus.Pending)
                throw new HandledExceptions("Booking have been done or deleted");
        }

        // Get date array for room plan
        private List<DateTime> GetDateArrange(DateTime checkinTime, DateTime checkoutTime)
        {
            var dates = new List<DateTime>();
            for (var dt = checkinTime; dt <= checkoutTime; dt = dt.AddDays(1))
            {
                dates.Add(dt);
            }

            return dates;
        }

        // Clear booking
        private async Task ClearBooking(int id)
        {
            if (id > 0)
            {
                var booking = await _bookingRepository.GetBookingById(id);
                ValidateBookingStatus(booking);

                _roomPlanRepository.RemoveBookingRoomPlan(id);

                booking.Status = BookingStatus.Done;
                _bookingRepository.Update(booking);
            }
        }

        // Add roomPlan for each room
        private void AddRoomPlan(int roomId, DateTime checkinTime, DateTime checkoutTime, Checkin checkin)
        {
            var dates = GetDateArrange(checkinTime, checkoutTime);

            var roomPlans = new List<RoomPlan>();
            foreach (var date in dates)
            {
                var roomPlan = new RoomPlan
                {
                    RoomId = roomId,
                    DateOcuppied = date,
                    Checkin = checkin,
                    RoomPlanType = RoomPlanType.Checkin,
                    RoomPlanStatus = RoomPlanStatus.Pending
                };
                roomPlans.Add(roomPlan);
            }
            _roomPlanRepository.AddRange(roomPlans);
        }
        #endregion
    }
}
