﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HotelManagement.Domain.Entities;
using HotelManagement.Domain.Interfaces;
using HotelManagement.Domain.RepositoryInterfaces;
using HotelManagement.Infrastructure.Repositories;
using HotelManagementAPI.ViewModels;

namespace HotelManagementAPI.Services
{
    public class ClientServices
    {
        private readonly IClientRepository _clientRepository;
        private readonly IUnitOfWork _unitOfWork;
        public ClientServices(IUnitOfWork unitOfWork, IClientRepository clientRepository)
        {
            _unitOfWork = unitOfWork;
            _clientRepository = clientRepository;
        }

        public async Task<MessageModel<int>> Add(ClientViewModel clientVM)
        {
            try
            {
                var client = CreateClient(clientVM);
                client.CreatedAt = DateTime.Now;
                
                _clientRepository.Add(client);
                _unitOfWork.SaveChanges();

                return new MessageModel<int> 
                { 
                    IsSuccess = true, 
                    Message = "Create client success", 
                    Data = client.Id 
                };
            }
            catch (Exception ex)
            {
                return new MessageModel<int> 
                { 
                    IsSuccess = false, 
                    Message = ex.ToString() 
                };
            }            
        }

        public async Task<MessageModel> Update(ClientViewModel clientVM, int clientId)
        {
            var client = await _clientRepository.GetClient(clientId);

            client.FullName = clientVM.FullName;
            client.Address = clientVM.Address;
            client.Email = clientVM.Email;
            client.IdentityCode = clientVM.IdentityCode;
            client.Phone = clientVM.PhoneNumber;
            client.Nationality = clientVM.Nationality;
            client.Notes = clientVM.Notes;
            client.UpdatedAt = DateTime.Now;

            _clientRepository.Update(client);
            _unitOfWork.SaveChanges();

            return new MessageModel 
            { 
                IsSuccess = true, 
                Message = "Update client success" 
            };
        }

        public async Task<Client> GetClient(int id)
        {
            return await _clientRepository.GetClient(id);
        }

        public async Task<PaginateDataResponse<Client>> GetClients(ClientFilter clientFilter)
        {
            List<Client> clients = await _clientRepository.GetClients(clientFilter.PageIndex, clientFilter.PageSize, clientFilter.Search);
            PaginateDataResponse<Client> paginateClients = new PaginateDataResponse<Client>
            {
                TotalItem = clients.Count(),
                Data = clients,
                PageIndex = clientFilter.PageIndex,
                PageSize = clientFilter.PageSize
            };

            return paginateClients;
        }

        private Client CreateClient(ClientViewModel clientVM)
        {
            var client = new Client
            {
                FullName = clientVM.FullName,
                Address = clientVM.Address,
                Email = clientVM.Email,
                IdentityCode = clientVM.IdentityCode,
                Phone = clientVM.PhoneNumber,
                Nationality = clientVM.Nationality,
                Notes = clientVM.Notes,
            };

            return client;
        }
    }
}
