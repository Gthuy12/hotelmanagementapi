﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using HotelManagementAPI.ViewModels;
using System.Threading.Tasks;
using HotelManagementAPI.Services;
using System.Security.Claims;
using HotelManagementAPI.Exceptions;
using Microsoft.AspNetCore.Authorization;
using HotelManagement.Domain.Entities;
using System.Collections.Generic;
using System;

namespace HotelManagementAPI.Controllers
{
    [Route("api/checkin")]
    [ApiController]
    public class CheckinController : ControllerBase
    {
        private readonly CheckinServices _checkinServices;
        private readonly RoomPlanServices _roomPlanServices; 

        public CheckinController(CheckinServices checkinServices, RoomPlanServices roomPlanServices)
        {
            _checkinServices = checkinServices;
            _roomPlanServices = roomPlanServices;
        }
        // Create Personal Checkin
        [CustomExceptionFilter]
        [HttpPost]
        [Authorize]
        public async Task<ActionResult<MessageModel<int>>> CheckinReservation([FromBody] CheckinViewModel checkin)
        {
            var authorId = HttpContext.User.FindFirstValue("ID");
            var checkinResult = await _checkinServices.CheckinPersonal(checkin, authorId);

            return StatusCode(
                StatusCodes.Status200OK,
                new MessageModel<int>
                {
                    IsSuccess = true,
                    Message = "Create new checkin success",
                    Data = checkinResult,
                });
        } // transaction open close commit

        // Create Group check-in
        [HttpPost]
        [Route("group")]
        [CustomExceptionFilter]
        public async Task<ActionResult<MessageModel>> GroupCheckin([FromBody] CheckinViewModel checkin)
        {
            var authorId = HttpContext.User.FindFirstValue("ID");
            var checkinResult = await _checkinServices.CheckinGroup(checkin, authorId);

            return StatusCode(
                StatusCodes.Status200OK,
                new MessageModel<int>
                {
                    IsSuccess = true,
                    Message = checkinResult.Message,
                });
        }

        [HttpPost]
        [Route("dailyaudit")]
        public async Task<ActionResult<MessageModel>> CreateDailyAudit()
        {
            // Add author to daily audit
            var auditResult = await _roomPlanServices.DailyAudit();

            return StatusCode(
                StatusCodes.Status200OK,
                new MessageModel<int>
                {
                    IsSuccess = auditResult.IsSuccess,
                    Message = auditResult.Message,
                });
        }

        [HttpPost]
        [Route("dailyaudit/{id:int}")]
        public async Task<ActionResult<MessageModel>> CreateDailyAuditForCheckin(int id)
        {
            // Add author to daily audit
            var auditResult = await _roomPlanServices.DailyAuditCheckin(id, DateTime.Now);

            return StatusCode(
                StatusCodes.Status200OK,
                new MessageModel<int>
                {
                    IsSuccess = auditResult.IsSuccess,
                    Message = auditResult.Message,
                });
        }

        // Get check-in detail
        [HttpGet] 
        [Route("{id:int}")]
        public async Task<ActionResult<Checkin>> GetCheckinDetail(int id)
        {
            var checkin = await _checkinServices.GetCheckin(id);
            return StatusCode(StatusCodes.Status200OK, checkin);
        }

        // Get preview invoices
        [HttpGet]
        [Route("preview/{id:int}")]
        public async Task<ActionResult<PreviewInvoiceViewModel>> GetPreviewInvoice(int id)
        {
            //Get all audit of this checkin
            var preview = await _roomPlanServices.GetPreviewInvoice(id);

            return StatusCode(StatusCodes.Status200OK, preview);
        }

        [HttpPut]
        [Route("service/{id:int}")]
        public async Task<ActionResult<MessageModel>> UpdateRoomServices([FromBody] RoomPlanModel roomPlanVM, int id)
        {
            var updateRoomServices = await _roomPlanServices.UpdateRoomServices(roomPlanVM, id);

            return StatusCode(StatusCodes.Status200OK, new MessageModel<int>
            {
                IsSuccess = true,
                Message = updateRoomServices.Message,
            });
        }

        // Changeroom for check-in
        [HttpPut]
        [Route("changeroom/{id:int}")]
        public async Task<ActionResult<MessageModel>> ChangeRoom([FromBody] CheckinViewModel checkinVM, int id)
        {
            var updateResult = await _roomPlanServices.ChangeRoomForCheckin(checkinVM, id, checkinVM.RoomId);

            return StatusCode(
               StatusCodes.Status200OK,
               new MessageModel
               {
                   IsSuccess = updateResult.IsSuccess,
                   Message = updateResult.Message,
               });
        }

        // Update check-in information
        [HttpPut]
        [Route("roomplan/{id:int}")]
        public async Task<ActionResult<MessageModel>> UpdateRoomPlan([FromBody] CheckinViewModel checkinVM, int id)
        {
            var updateResult = await _roomPlanServices.UpdateRoomPlan(checkinVM, id);

            return StatusCode(
                StatusCodes.Status200OK,
                new MessageModel
                {
                    IsSuccess = updateResult.IsSuccess,
                    Message = updateResult.Message,
                });
        }

        [HttpGet]
        [Route("roomplan")]
        public async Task<ActionResult<List<RoomPlan>>> RetriveRoomPlans([FromQuery] RoomPlanFilter roomPlanFilter)
        {
            var roomPlans = await _roomPlanServices.GetRoomPlans(roomPlanFilter);

            return StatusCode(StatusCodes.Status200OK, roomPlans);
        }

        [HttpGet]
        [Route("checkin")]
        public async Task<ActionResult<List<RoomPlan>>> RetriveCheckins([FromQuery] CheckinFilter checkinFilter)
        {
            var checkins = await _checkinServices.GetCheckins(checkinFilter);

            return StatusCode(StatusCodes.Status200OK, checkins);
        }

        //Delete booking
        [HttpDelete]
        [Route("{id:int}")]
        public async Task<ActionResult<MessageModel>> DeleteBooking(int id)
        {
            return StatusCode(
                StatusCodes.Status200OK,
                new MessageModel
                {
                    IsSuccess = true,
                    Message = "Delete checkin success"
                });
        }                        
    }
}
