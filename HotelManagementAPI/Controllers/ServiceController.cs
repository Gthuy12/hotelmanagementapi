﻿using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using HotelManagement.Domain.Entities;
using HotelManagementAPI.ViewModels;
using HotelManagementAPI.Services;

namespace HotelManagementAPI.Controllers
{
    [Route("api/service")]
    [ApiController]
    public class ServiceController : ControllerBase
    {
        private readonly ServiceServices _serviceServices;

        public ServiceController(ServiceServices serviceServices)
        {
            _serviceServices = serviceServices;
        }

        [HttpGet]
        [Route("{id:int}")]
        public async Task<Service> GetRoom(int id)
        {
            return _serviceServices.GetService(id);
        }

        [HttpPut]
        [Route("{id:int}")]
        public async Task<MessageModel> UpdateRoom(int serviceId, [FromBody] ServiceViewModel serviceVM)
        {
            return _serviceServices.Update(serviceId, serviceVM);
        }

        [HttpPost]
        public MessageModel AddRoom([FromBody] ServiceViewModel serviceVM)
        {
            return _serviceServices.Add(serviceVM);
        }
    }
}
