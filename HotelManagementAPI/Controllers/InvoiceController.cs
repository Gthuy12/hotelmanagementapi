﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HotelManagement.Domain.Entities;
using HotelManagementAPI.ViewModels;
using HotelManagementAPI.Services;

namespace HotelManagementAPI.Controllers
{
    [Route("api/invoice")]
    [ApiController]
    public class InvoiceController : ControllerBase
    {
        private readonly InvoiceServices _invoiceServices;

        public InvoiceController(InvoiceServices invoiceServices)
        {
            _invoiceServices = invoiceServices;
        }

        // Get Invoice detail
        [HttpGet]
        public async Task<ActionResult<PaginateDataResponse<Invoice>>> GetInvoices([FromQuery] InvoiceFilter filter)
        {
            var invoices = await _invoiceServices.GetInvoices(filter);

            return StatusCode(StatusCodes.Status200OK, invoices);
        }

        [HttpGet]
        [Route("{id:int}")]
        public async Task<ActionResult<PaginateDataResponse<Invoice>>> GetInvoice(int id)
        {
            var invoice = await _invoiceServices.GetInvoice(id);

            return StatusCode(StatusCodes.Status200OK, invoice);
        }

        // Update invoice info
        [HttpPut]
        [Route("{id:int}")]
        public async Task<ActionResult<MessageModel>> UpdateInvoice(int id)
        {
            var updateResult = await _invoiceServices.SetInvoiceToDone(id);

            return StatusCode(StatusCodes.Status200OK, new MessageModel
            {
                IsSuccess = updateResult.IsSuccess,
                Message = updateResult.Message,
            });
          
        }

        // Delete invoice 
        [HttpPut]
        [Route("delete/{id:int}")]
        public async Task<ActionResult<MessageModel>> DeleteInvoice(int id)
        {
            var deleteResult = await _invoiceServices.DeleteInvoice(id);

            return StatusCode(StatusCodes.Status200OK, new MessageModel
            {
                IsSuccess = deleteResult.IsSuccess,
                Message = deleteResult.Message,
            });
        }
    }
}
