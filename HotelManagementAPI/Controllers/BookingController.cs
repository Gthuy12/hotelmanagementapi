﻿
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using HotelManagementAPI.ViewModels;
using HotelManagementAPI.Services;
using HotelManagement.Domain.Entities;
using HotelManagementAPI.Exceptions;
namespace HotelManagementAPI.Controllers

{
    [Route("api/booking")]
    [ApiController]
    public class BookingController : ControllerBase
    {
        private readonly BookingServices _bookingServices;
        public BookingController(BookingServices bookingService)
        {
            _bookingServices = bookingService;
        }
        // Create new booking for personal and group
        // 1. Include create list services
        [HttpPost]
        [Authorize]
        [CustomExceptionFilter]
        public async Task<ActionResult<MessageModel>> Booking([FromBody] BookingViewModel reservation)
        {
            var authorId = HttpContext.User.FindFirstValue("ID");
            var addBooking = await _bookingServices.CreateReservation(reservation, authorId);

            return StatusCode(
                StatusCodes.Status200OK, 
                new MessageModel {
                    IsSuccess = true, 
                    Message = addBooking.Data.ToString() 
                });
        }

        // Get booked clients list
        [CustomExceptionFilter]
        [HttpGet]
        public async Task<ActionResult<PaginateDataResponse<Booking>>> GetBookings([FromQuery] BookingFilter filter)
        {
            var bookings = await _bookingServices.GetBookings(filter);

            return StatusCode(StatusCodes.Status200OK, bookings);
        }

        // Get booked detail by id
        [CustomExceptionFilter]
        [HttpGet]
        [Route("{id:int}")] 
        public async Task<ActionResult<Booking>> GetBookedDetails(int id)
        {
            var booking = await _bookingServices.GetBooking(id);
            return StatusCode(StatusCodes.Status200OK, booking);
        }

        //Get booked detail by code
        [CustomExceptionFilter]
        [HttpGet]
        [Route("{code}")]
        public async Task<ActionResult<BookingViewModel>> GetBookedDetails(string code)
        {
            return new BookingViewModel();
        }

        //Update booking infomation
        [CustomExceptionFilter]
        [HttpPut]
        [Route("{id:int}")]
        public async Task<ActionResult<MessageModel>> UpdateBooking([FromBody] BookingViewModel bookingVM ,int id)
        {
            var updateResult = await _bookingServices.UpdateBooking(bookingVM, id);

            return StatusCode(
                StatusCodes.Status200OK,
                new MessageModel
                {
                    IsSuccess = updateResult.IsSuccess,
                    Message = updateResult.Message
                });
        }

        //Delete booking
        [CustomExceptionFilter]
        [HttpPut]
        [Route("delete/{id:int}")]
        public async Task<ActionResult<MessageModel>> DeleteBooking(int id)
        {
            await _bookingServices.DeleteBooking(id);
            return StatusCode(
                StatusCodes.Status200OK,
                new MessageModel
                {
                    IsSuccess = true,
                    Message = "Delete booking success"
                });
        }
    }
}
