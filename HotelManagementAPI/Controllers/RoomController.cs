﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HotelManagement.Domain.Entities;
using HotelManagementAPI.ViewModels;
using HotelManagementAPI.Services;

namespace HotelManagementAPI.Controllers
{
   
    [Route("api/rooms")]
    [ApiController]
    public class RoomController : ControllerBase
    {
        private readonly RoomServices _roomServices;
        public RoomController(RoomServices roomServices)
        {
            _roomServices = roomServices;
        }
        // Retrive rooms by filter
        [HttpGet]
        public async Task<RoomResponseData> GetRooms([FromQuery] RoomFilters filter)
        {
            return await _roomServices.GetRooms(filter);
        }

        [HttpGet]
        [Route("available")]
        public async Task<RoomResponseData> GetRoomByDate([FromQuery] RoomFilters filter)
        {
            return await _roomServices.GetRoomAvaibleDate(filter);
        }

        // Get room detail information
        [HttpGet]
        [Route("{id:int}")]
        public async Task<Room> GetRoom(int id)
        {
            return await _roomServices.GetRoom(id);
        } 

        // Update room information
        [HttpPut]
        [Route("{id:int}")]
        public async Task<MessageModel> UpdateRoom(int id, [FromBody] RoomViewModel room)
        {
            return await _roomServices.Update(id, room);
        }

        [HttpPost]
        public async Task<MessageModel> AddRoom([FromBody] RoomViewModel room)
        {
            return await _roomServices.Add(room);
        }
    }
}
