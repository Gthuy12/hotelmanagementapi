﻿using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using HotelManagementAPI.ViewModels;
using HotelManagement.Domain.Entities;
using HotelManagementAPI.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Authorization;

namespace HotelManagementAPI.Controllers
{
   
    [Route("api/clients")]
    [ApiController]
    public class ClientsController : ControllerBase
    {
        private readonly ClientServices _clientServices;
        public ClientsController(ClientServices clientServices)
        {
            _clientServices = clientServices;
        }
        // Create a client used for create booking, check-in
        [HttpPost]
        [Authorize]
        public async Task<ActionResult<MessageModel>> CreateClient([FromBody] ClientViewModel clientVM)
        {
            var addClient = await _clientServices.Add(clientVM);

            return addClient.IsSuccess ?
               StatusCode(StatusCodes.Status200OK, new MessageModel { IsSuccess = true, Message = addClient.Data.ToString() })
               : StatusCode(StatusCodes.Status400BadRequest, new MessageModel { IsSuccess = false, Message = "Can not create client" });              
        }

        // Get client information when retrive a detail of booking, check-in or invoice
        [HttpGet]
        [Route("{id:int}")]
        public async Task<ActionResult<Client>> GetClient(int id)
        {
            var client = await _clientServices.GetClient(id);
            return StatusCode(StatusCodes.Status200OK, client);
        }

        // Retrive many clients
        [HttpGet]
        public async Task<ActionResult<PaginateDataResponse<Client>>> GetClients([FromQuery] ClientFilter filterClients)
        {
            var paginateClients = await _clientServices.GetClients(filterClients);

            return StatusCode(StatusCodes.Status200OK, paginateClients);
        }

        // Update client info
        [HttpPut]
        [Route("{id:int}")]
        public async Task<ActionResult<MessageModel>> UpdateClient([FromBody] ClientViewModel clientVM, int id)
        {
            var update = await _clientServices.Update(clientVM, id);

            return StatusCode(
                StatusCodes.Status200OK,
                new MessageModel
                {
                    IsSuccess = update.IsSuccess,
                    Message = update.Message,
                });
        }
    }
}
