﻿using System;
using System.Threading.Tasks;
using System.Text;
using System.Security.Claims;
using System.IdentityModel.Tokens.Jwt;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.Configuration;
using Microsoft.AspNetCore.Identity;
using HotelManagementAPI.ViewModels;
using HotelManagement.Domain.Entities;
using Microsoft.AspNetCore.Http;

namespace HotelManagementAPI.Controllers
{
    [Route("api/auth")]
    [ApiController]
    public class AuthenticationController : ControllerBase
    {
        private IConfiguration _config;
        private readonly UserManager<User> _userManager;
        public AuthenticationController(UserManager<User> userManager, IConfiguration config)
        {
            _userManager = userManager;
            _config = config;
        }
        //Login user
        [HttpPost]
        [Route("signin")]
        public async Task<ActionResult<MessageModel>> Login([FromBody] LoginForm body)
        {
            var user = await _userManager.FindByNameAsync(body.username);
            if(user == null || !await _userManager.CheckPasswordAsync(user, body.password))
            {
                return StatusCode(401, new MessageModel
                {
                    IsSuccess = false,
                    Message = "Username or password incorrect"
                }); 
            }
            var tokenString = GenerateJwt(user);
            return Ok(new { token = tokenString });
        }

        //Get user information by token
        // 1. AuthMe return user info require token in header
        // 2. Verify Token Middleware here
        [HttpGet]
        [Authorize]
        [Route("MyProfile")]
        public async Task<ActionResult<UserResponseData>> AuthMe()
        {
            var claim = HttpContext.User.FindFirstValue("ID");
            var user = await _userManager.FindByIdAsync(claim);
            return StatusCode(StatusCodes.Status200OK, new UserResponseData
            {
                Username = user.UserName,
                Email = user.Email,
                Status = user.Status
            });
        }

        private string GenerateJwt(User user)
        {
            var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_config["Jwt:Key"]));
            var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);

            var claims = new[]
            {
                new Claim("ID", user.Id.ToString())
            };

            var token = new JwtSecurityToken(
              _config["Jwt:Issuer"],
              _config["Jwt:Issuer"],
              claims,
              expires: DateTime.Now.AddMinutes(120),
              signingCredentials: credentials);
            return new JwtSecurityTokenHandler().WriteToken(token);
        }       
    }
}
