﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HotelManagementAPI.ViewModels;
using HotelManagementAPI.Services;
using HotelManagementAPI.Exceptions;
using Microsoft.AspNetCore.Authorization;
using System.Security.Claims;

namespace HotelManagementAPI.Controllers
{
    [Route("api/checkout")]
    [ApiController]
    public class CheckoutController : ControllerBase
    {
        private readonly CheckoutServices _checkoutServices;

        public CheckoutController(CheckoutServices checkoutServices)
        {
            _checkoutServices = checkoutServices;
        }

        [HttpPost]
        [Authorize]
        [CustomExceptionFilter]
        [Route("personal/{id:int}")]
        public async Task<ActionResult<MessageModel>> CreateCheckoutPersonal([FromBody] CheckoutViewModel checkoutVM, int id)
        {
            var authorId = HttpContext.User.FindFirstValue("ID");
            var checkout = await _checkoutServices.CheckoutPersonal(authorId, id, checkoutVM);

            return StatusCode(
                StatusCodes.Status200OK,
                new MessageModel
                {
                    IsSuccess = checkout.IsSuccess,
                    Message = checkout.Message,
                });
        }

        [HttpPost]
        [Route("group")]
        public async Task<ActionResult<MessageModel>> CreateCheckoutGroup([FromBody] CheckoutViewModel checkoutVM)
        {
            List<int> ids = new List<int>();
            var authorId = HttpContext.User.FindFirstValue("ID");
            var checkout = await _checkoutServices.CheckoutGroup(ids, authorId, checkoutVM);

            return StatusCode(
                StatusCodes.Status200OK,
                new MessageModel
                {
                    IsSuccess = checkout.IsSuccess,
                    Message = checkout.Message,
                });
        }
    }
}
