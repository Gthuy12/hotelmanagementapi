﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Identity;
using HotelManagementAPI.ViewModels;
using HotelManagement.Domain.Entities;



namespace HotelManagementAPI.Controllers
{
    public class UserForm
    {
        [Required]
        public string username { get; set; }
        [Required]
        public string email { get; set; }
        [Required]
        public string password { get; set; }
    }

    [Route("api/user")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly UserManager<User> _userManager;
        public UserController(UserManager<User> userManager)
        {
            _userManager = userManager;
        }
        [HttpPost]
        [Route("create")]
        public async Task<ActionResult<MessageModel>> Create([FromBody] UserForm form)
        {
            var userExist = await _userManager.FindByNameAsync(form.username);
            if(userExist != null)
            {
                return StatusCode(409, new MessageModel
                {
                    IsSuccess = false,
                    Message = "User already exist"
                });
            }
            var user = new User
            {
                Status = true,
                Email = form.email,
                SecurityStamp = Guid.NewGuid().ToString(),
                UserName = form.username
            };
            var createUser = await _userManager.CreateAsync(user, form.password);
            if(!createUser.Succeeded)
            {
                return StatusCode(500, new MessageModel
                {
                    IsSuccess = false,
                    Message = $"{createUser.Errors.ToList()[0].Description}"
                });
            }
            return Ok(new MessageModel
            {
                IsSuccess = true,
                Message = "Create new user success"
            });
        }
    }
}
