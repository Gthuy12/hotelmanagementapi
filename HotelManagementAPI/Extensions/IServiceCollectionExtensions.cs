﻿using System;
using System.Collections.Generic;
using HotelManagement.Infrastructure;
using HotelManagement.Domain.Interfaces;
using HotelManagement.Infrastructure.Repositories;
using HotelManagementAPI.Services;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.OpenApi.Models;
using HotelManagement.Domain.RepositoryInterfaces;

namespace HotelManagementAPI.Extensions
{
    public static class IServiceCollectionExtensions
    {
        public static IServiceCollection AddDatabase(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddDbContext<HotelDbContext>(options =>
            {
                options.UseSqlServer(configuration.GetConnectionString("cnn"));
                options.EnableSensitiveDataLogging(true);
            });
            services.AddScoped<DbFactory>();
            services.AddScoped<Func<HotelDbContext>>((provider) => () => provider.GetService<HotelDbContext>());
            services.AddScoped<IUnitOfWork, UnitOfWork>();

            return services;
        }

        public static IServiceCollection AddRepositories(this IServiceCollection services)
        {
            services
               .AddScoped(typeof(IRepository<>), typeof(Repository<>))
               .AddScoped<IRoomRepository, RoomRepository>()
               .AddScoped<IRoomPlanRepository, RoomPlanRepository>()
               .AddScoped<IRoomServiceRepository, RoomServiceRepository>()
               .AddScoped<IServiceRepository, ServiceRepository>()
               .AddScoped<IClientRepository, ClientRepository>()
               .AddScoped<IBookingRepository, BookingRepository>()
               .AddScoped<IBookingRoomRepository, BookingRoomRepository>()
               .AddScoped<ICheckinRepository, CheckinRepository>()
               .AddScoped<ICheckinServiceRepository, CheckinServiceRepository>()
               .AddScoped<IDailyAuditRepository, DailyAuditRepository>()
               .AddScoped<ICheckoutRepository, CheckoutRepository>()
               .AddScoped<IInvoiceRepository, InvoiceRepository>()
               .AddScoped<IInvoiceServiceRepository, InvoiceServiceRepository>();

            return services;
        }

        public static IServiceCollection AddServices(this IServiceCollection services)
        {
            return services
                .AddScoped<RoomServices>()
                .AddScoped<ServiceServices>()
                .AddScoped<ClientServices>()
                .AddScoped<BookingServices>()
                .AddScoped<CheckinServices>()
                .AddScoped<CheckoutServices>()
                .AddScoped<RoomPlanServices>()
                .AddScoped<InvoiceServices>();
        }

        public static IServiceCollection ConfigureSwagger(this IServiceCollection services)
        {
            return services
                .AddSwaggerGen(c =>
                {
                    c.SwaggerDoc("v1", new OpenApiInfo
                    {
                        Version = "v1",
                        Title = "HotelManagementAPI",
                        Description = "HOTEL MANAGEMENT API"
                    });

                    c.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme
                    {
                        Description = "JWT Authorization header using the Bearer scheme",
                        Name = "Authorization",
                        In = ParameterLocation.Header,
                        Type = SecuritySchemeType.ApiKey,
                        Scheme = "Bearer"
                    });

                    c.AddSecurityRequirement(new OpenApiSecurityRequirement()
                    {
                        {
                            new OpenApiSecurityScheme
                            {
                                Reference = new OpenApiReference
                                {
                                    Type = ReferenceType.SecurityScheme,
                                    Id = "Bearer"
                                },
                                Scheme = "oauth2",
                                Name = "Bearer",
                                In = ParameterLocation.Header,
                            },
                            new List<string>()
                        }
                    });
                });
        }
    }
}
