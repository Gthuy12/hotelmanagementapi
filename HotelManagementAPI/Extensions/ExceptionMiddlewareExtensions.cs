﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Http;
using System.Net;
using HotelManagementAPI.ViewModels;
using Newtonsoft.Json;

namespace HotelManagementAPI.Extensions
{
    public class ExceptionMiddlewareExtensions
    {
        private readonly RequestDelegate _next;

        public ExceptionMiddlewareExtensions(RequestDelegate next)
        {
            _next = next;
        }
        public async Task Invoke(HttpContext context)
        {
            try
            {
                await _next.Invoke(context);
            }
            catch (Exception ex)
            {
                await HandleExceptionAsync(context, ex);
            }
        }
        private static Task HandleExceptionAsync(HttpContext context, Exception exception)
        {
            context.Response.ContentType = "application/json";
            int statusCode = (int)HttpStatusCode.InternalServerError;
            var result = JsonConvert.SerializeObject(new
            {
                StatusCode = statusCode,
                ErrorMessage = exception.Message
            });
            context.Response.ContentType = "application/json";
            context.Response.StatusCode = statusCode;
            return context.Response.WriteAsync(result);
        }

        // There are two ways to define a Exception Middleware to handle global Exceptions
        // 1. Use the UseExceptionHandler middleware
        // 2. Custom a ExceptionMiddleware (above)
        // Link: https://www.c-sharpcorner.com/article/implement-global-exception-handling-in-asp-net-core-application/
        //       https://code-maze.com/global-error-handling-aspnetcore/

        // Middleware work with HttpContext (header, body, IP, ...), so you should use it to handle error outside logic block
        // Exception Filter, Action Filter work with you Logic block, use it to handle exceptions inside your logic handle
        // Register exception filter as global and inside of it, implement logic to handle all exceptions (401,404,500, ...) 
    }
}
