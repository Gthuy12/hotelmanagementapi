﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HotelManagement.Domain.Interfaces
{
    public interface IUnitOfWork
    {
        void SaveChanges();
    }
}
