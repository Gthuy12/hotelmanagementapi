﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace HotelManagement.Domain.Interfaces
{
    public interface IRepository<T> where T : class
    {
        void SetState(T entity, EntityState state);
        void Add(T entity);
        void Update(T entity);
        void Delete(T entity);
        void AddRange(List<T> entities);
        void RemoveRange(List<T> entities);
    }
}
