﻿using HotelManagement.Domain.Entities;
using HotelManagement.Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace HotelManagement.Domain.RepositoryInterfaces
{
    public interface ICheckoutRepository : IRepository<Checkout>
    {
    }
}
