﻿using HotelManagement.Domain.Entities;
using HotelManagement.Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HotelManagement.Domain.RepositoryInterfaces
{
    public interface IRoomPlanRepository : IRepository<RoomPlan>
    {
        IQueryable<RoomPlan> GetRoomPlans();
        IQueryable<RoomPlan> GetRooms(List<Room> rooms, List<DateTime> dateTimes, RoomPlanType planType);
        IQueryable<RoomPlan> GetRoom(int id, List<DateTime> dateTimes, RoomPlanType planType);
        void RemoveBookingRoomPlan(int id);
        void UpdateRoomPlans(List<RoomPlan> roomPlans);
        void RemoveRoomPlan(int checkinId, DateTime date);
    }
}
