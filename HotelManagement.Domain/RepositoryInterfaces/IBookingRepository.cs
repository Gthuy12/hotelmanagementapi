﻿using HotelManagement.Domain.Entities;
using HotelManagement.Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HotelManagement.Domain.RepositoryInterfaces
{
    public interface IBookingRepository : IRepository<Booking>
    {
        Task<Booking> GetBookingByCode(string code);
        Task<Booking> GetBookingById(int id);
        IQueryable<Booking> QueryBooking(DateTime checkin, DateTime checkout);
    }
}
