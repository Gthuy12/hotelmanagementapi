﻿using HotelManagement.Domain.Entities;
using HotelManagement.Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HotelManagement.Domain.RepositoryInterfaces
{
    public interface ICheckinRepository : IRepository<Checkin>
    {
        IQueryable<Checkin> GetCheckin(int checkinId);
        IQueryable<Checkin> GetCheckins();
        IQueryable<Checkin> GetDepositCheckin(Invoice invoice);
    }
}
