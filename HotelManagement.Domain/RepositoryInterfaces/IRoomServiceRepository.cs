﻿using HotelManagement.Domain.Entities;
using HotelManagement.Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace HotelManagement.Domain.RepositoryInterfaces
{
    public interface IRoomServiceRepository : IRepository<RoomService>
    {
        void AddServices(List<RoomService> roomServices);
        void RemoveRoomServices(int roomPlanId);
    }
}
