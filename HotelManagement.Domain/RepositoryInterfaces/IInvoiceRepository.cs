﻿using HotelManagement.Domain.Entities;
using HotelManagement.Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HotelManagement.Domain.RepositoryInterfaces
{
    public interface IInvoiceRepository : IRepository<Invoice>
    {
        IQueryable<Invoice> GetInvoices(InvoiceStatus status);
        IQueryable<Invoice> GetInvoice(int id);
        IQueryable<Invoice> GetDepositInvoice(Invoice invoice);
    }
}
