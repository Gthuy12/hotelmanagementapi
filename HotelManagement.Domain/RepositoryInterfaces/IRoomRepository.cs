﻿using HotelManagement.Domain.Entities;
using HotelManagement.Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace HotelManagement.Domain.RepositoryInterfaces
{
    public interface IRoomRepository : IRepository<Room>
    {
        Task<Room> GetRoom(int id);
        Task<List<Room>> GetRooms(int floor, RoomStatus status, string search);
        Task<List<Room>> GetRooms(DateTime checkin, DateTime checkout, string search);
        Task<List<Room>> GetRoomByDate(DateTime beginDate, DateTime endDate);
        Task<List<Room>> GetUnavailableRooms();
    }
}
