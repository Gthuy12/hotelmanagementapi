﻿using HotelManagement.Domain.Entities;
using HotelManagement.Domain.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace HotelManagement.Domain.RepositoryInterfaces
{
    public interface ICheckinServiceRepository : IRepository<CheckinService>
    {
        Task<List<CheckinService>> GetCheckinServices(int id);

        void DeleteCheckinServices(int id);
    }
}
