﻿using HotelManagement.Domain.Entities;
using HotelManagement.Domain.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace HotelManagement.Domain.RepositoryInterfaces
{
    public interface IClientRepository : IRepository<Client>
    {
        Task<Client> GetClient(int clientId);
        Task<List<Client>> GetClientsByIds(List<int> clientIds);
        Task<List<Client>> GetClients(int pageIndex, int pageSize, string search);
    }
}
