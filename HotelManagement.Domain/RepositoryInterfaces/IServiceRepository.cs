﻿using HotelManagement.Domain.Entities;
using HotelManagement.Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace HotelManagement.Domain.RepositoryInterfaces
{
    public interface IServiceRepository : IRepository<Service>
    {
        Service GetService(int serviceId);
        Task<List<Service>> GetServices(List<int> serviceIds);
    }
}
