﻿using HotelManagement.Domain.Entities;
using HotelManagement.Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HotelManagement.Domain.RepositoryInterfaces
{
    public interface IDailyAuditRepository : IRepository<DailyAudit>
    {
        void AddDailyAudits(List<DailyAudit> audits);
        void RemoveDailyAudits(DateTime date);
        IQueryable<DailyAudit> GetDailyAudits(List<int> roomPlanIds);
        void RemoveDailyAudit(DateTime date, int id);
    }
}
