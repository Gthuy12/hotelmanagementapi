﻿using HotelManagement.Domain.Entities;
using HotelManagement.Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace HotelManagement.Domain.RepositoryInterfaces
{
    public interface IInvoiceServiceRepository : IRepository<InvoiceService>
    {
        void DeleteInvoiceServices(int id);
    }
}
