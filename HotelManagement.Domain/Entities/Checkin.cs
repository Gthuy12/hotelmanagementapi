﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HotelManagement.Domain.Entities
{
    public enum CheckinStatus
    {
        Pending, Done, Delete
    }

    [Table("Checkin")]
    public class Checkin
    {
        public Checkin()
        {
            this.Invoices = new HashSet<Invoice>();
            this.Clients = new HashSet<Client>();
            this.RoomPlans = new HashSet<RoomPlan>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public DateTime CheckinTime { get; set; }
        public DateTime CheckoutTime { get; set; }
        public string ContactName { get; set; }
        public string ContactPhone { get; set; }
        public string Notes { get; set; }
        public decimal Prepay { get; set; }
        public decimal Discount { get; set; }
        public CheckinStatus Status { get; set; }
        public int RoomId { get; set; }
        [ForeignKey(nameof(RoomId))]
        public virtual Room Room { get; set; }
        public string CreatedByAuthor { get; set; }

        [ForeignKey(nameof(CreatedByAuthor))]
        public virtual User CreatedBy { get; set; }
        public virtual ICollection<Client> Clients { get; set; }
        public virtual ICollection<Invoice> Invoices { get; set; }
        public virtual ICollection<RoomPlan> RoomPlans { get; set; }
    }
}
