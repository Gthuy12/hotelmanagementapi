﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace HotelManagement.Domain.Entities
{
    [Table("Client")]
    public partial class Client
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public string FullName { get; set; }
        public string Address { get; set; }
        public string Email { get; set; }
        public string IdentityCode { get; set; }
        public string Phone { get; set; }
        public string Nationality { get; set; }
        public string Notes { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime? UpdatedAt { get; set; }
        public DateTime? DeletedAt { get; set; }
        public virtual ICollection<Booking> Bookings { get; set; }
        public virtual ICollection<Checkin> Checkins { get; set; }
        public Client()
        {
            this.Checkins = new HashSet<Checkin>();
            this.Bookings = new HashSet<Booking>();
        }
    }
}
