﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace HotelManagement.Domain.Entities
{
    public enum InvoiceStatus
    {
        Pending, Done, Debt, Deposit
    }

    [Table("Invoice")]
    public partial class Invoice
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public string Code { get; set; }
        public InvoiceStatus Status { get; set; }
        public decimal Prepay { get; set; }
        public decimal Discount { get; set; }
        public decimal RoomPrice { get; set; }
        public decimal ServicePrice { get; set; }
        public decimal TotalPrice { get; set; }
        public int CheckoutId { get; set; }
        [ForeignKey(nameof(CheckoutId))]
        public virtual Checkout Checkout { get; set; }
        public string AuthorId { get; set; }
        [ForeignKey(nameof(AuthorId))]
        public virtual User User { get; set; }
        public virtual ICollection<Invoice> DepositInvoices { get; set; } // When create invoice check child invoices and
        public virtual ICollection<InvoiceDetail> InvoiceDetails { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime? UpdatedAt { get; set; }
        public DateTime? DeletedAt { get; set; }

        public Invoice()
        {
            DepositInvoices = new HashSet<Invoice>();
            InvoiceDetails = new HashSet<InvoiceDetail>();
        }
    }
}
