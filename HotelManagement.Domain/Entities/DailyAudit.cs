﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace HotelManagement.Domain.Entities
{
    [Table("DailyAudit")]
    public class DailyAudit
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public DateTime DateOcuppied { get; set; }
        public virtual RoomPlan RoomPlan { get; set; }
        public decimal ServicePrice { get; set; }
        public decimal RoomPrice { get; set; }
        public decimal TotalPrice { get; set; }
    }
}
