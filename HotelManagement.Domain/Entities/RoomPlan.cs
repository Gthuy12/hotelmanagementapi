﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace HotelManagement.Domain.Entities
{
    public enum RoomPlanStatus
    {
        Pending, Done
    }
    public enum RoomPlanType
    {
        Booking, Checkin
    }

    [Table("RoomPlan")]
    public class RoomPlan
    {
        public RoomPlan()
        {
            Services = new HashSet<RoomService>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public int RoomId { get; set; }
        [ForeignKey(nameof(RoomId))]
        public virtual Room Room { get; set; }
        public int? BookingId { get; set; }
        [ForeignKey(nameof(BookingId))]
        public virtual Booking Booking { get; set; }
        public int? CheckinId { get; set; }
        [ForeignKey(nameof(CheckinId))]
        public virtual Checkin Checkin { get; set; }
        public DateTime DateOcuppied { get; set; }
        public RoomStatus Status { get; set; }  // room status
        public RoomPlanType RoomPlanType { get; set; }
        public RoomPlanStatus RoomPlanStatus { get; set; }
        public virtual ICollection<RoomService> Services { get; set; }
    }
}
