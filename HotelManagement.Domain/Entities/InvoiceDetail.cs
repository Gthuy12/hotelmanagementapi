﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace HotelManagement.Domain.Entities
{
    [Table("InvoiceDetail")]
    public class InvoiceDetail
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public DateTime DateOcuppied { get; set; }
        public int InvoiceId { get; set; }
        [ForeignKey(nameof(InvoiceId))]
        public virtual Invoice Invoice { get; set; }
        public virtual DailyAudit DailyAudit { get; set; }
        public decimal ServicePrice { get; set; }
        public decimal RoomPrice { get; set; }
        public decimal TotalPrice { get; set; }
    }
}
