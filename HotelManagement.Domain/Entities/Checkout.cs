﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace HotelManagement.Domain.Entities
{
    [Table("Checkout")]
    public partial class Checkout
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public DateTime CheckinTime { get; set; }
        public DateTime CheckoutTime { get; set; }
        public int CheckinId { get; set; }
        [ForeignKey(nameof(CheckinId))]
        public virtual Checkin Checkin { get; set; }
        public string AuthorId { get; set; }

        [ForeignKey(nameof(AuthorId))]
        public virtual User User { get; set; }
    }
}
