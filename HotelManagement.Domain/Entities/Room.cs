﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace HotelManagement.Domain.Entities
{
    public enum RoomType
    {
        Single, Double
    }

    public enum RoomStatus
    {
        Available, Ocuppied, Dirty, Cleaning, All
    }

    [Table("Room")]
    public partial class Room
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public string RoomCode { get; set; }
        public RoomType Type { get; set; }
        public RoomStatus Status { get; set; }
        public decimal Price { get; set; }
        public int Floor { get; set; }
        public ICollection<Booking> Bookings { get; set; }
        public Room()
        {
            this.Bookings = new HashSet<Booking>();
        }
    }
}
