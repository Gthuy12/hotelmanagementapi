﻿using Microsoft.AspNetCore.Identity;
using System.Collections.Generic;

namespace HotelManagement.Domain.Entities
{
    public class User : IdentityUser
    {      
        public bool Status { get; set; } 
        public virtual ICollection<Booking> Bookings { get; set; }
    }
}
