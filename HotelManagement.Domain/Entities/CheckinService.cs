﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace HotelManagement.Domain.Entities
{
    [Table("CheckinService")]
    public partial class CheckinService
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public int CheckinId { get; set; }
        [ForeignKey(nameof(CheckinId))]
        public virtual Checkin Checkin { get; set; }
        public int ServiceId { get; set; }
        [ForeignKey(nameof(ServiceId))]
        public virtual Service Service { get; set; }
        public int Quantity { get; set; }
        public decimal Price { get; set; }
    }
}
