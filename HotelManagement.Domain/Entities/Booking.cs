﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace HotelManagement.Domain.Entities
{
    public enum BookingStatus
    {
        Pending, Done, Deleted
    }
    [Table("Booking")]
    public partial class Booking
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public string Code { get; set; }
        public DateTime CheckinTime { get; set; }
        public DateTime CheckoutTime { get; set; }
        public decimal Prepay { get; set; }
        public decimal Discount { get; set; }
        public BookingStatus Status { get; set; }
        public string Notes { get; set; }
        public string ContactName { get; set; }
        public string ContactPhone { get; set; }
        public string CreatedByAuthor { get; set; }

        [ForeignKey(nameof(CreatedByAuthor))]
        public virtual User CreatedBy { get; set; }
        public virtual ICollection<Client> Clients { get; set; }
        public virtual ICollection<BookingRoom> RoomQuantities { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime? UpdatedAt { get; set; }
        public DateTime? DeletedAt { get; set; }
        public Booking()
        {
            this.Clients = new HashSet<Client>();
            this.RoomQuantities = new HashSet<BookingRoom>();
        }       
    }
}
